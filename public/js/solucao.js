const apiSolucao = "https://api-vertiv-biz.mybluemix.net/api/SolutionBox/mobile";
let selector = null;
const settingsListSolucao = {
    "async": true,
    "crossDomain": true,
    "url": `${apiSolucao}?ppage=1&pquantidade=20`,
    "method": "GET",
    "headers": {
        "Cache-Control": "no-cache",
        "authorization": `bearer ${JSON.parse(localStorage.getItem('token')).token}`
    }
};

$(document).ready(function () {
    selector = '#lista-solucoes';
    $('.modal').modal();
    $('#app-action').html('');

    setTitle('Soluções');
    $.ajax(settingsListSolucao).done(function (response) {
        //loadingList(selector);
        populateList(response);
    });
    $('select').material_select();
});


function filterList() {
    const filter = $('#filter-input').val();
    sessionStorage.setItem('filter', filter);
    $(selector).html('');

    loadDataFromServer(filter);
}

function loadDataFromServer(filter) {
    settingsListSolucao.url = `${settingsListSolucao.url}&filter=${filter}`;
    $.ajax(settingsListSolucao).done(function (response) {
        populateList(response);
        //getServerListPagination($('#pagnination'), $('#produtos'), api, response.quantidade, 10, populateTable, showError);
        Materialize.updateTextFields();
        //$('#pages').on('change', function () {
        //    paginate(api, 20);
        //}).material_select();
    });
    settingsListSolucao.url = `${apiSolucao}?ppage=1&pquantidade=20`
}

function populateList(response) {
    $.each(response.solutionBoxList, function (i, item) {
        let image = item.imageUrl;
        if (image === null || image === '' || image === 'undefined')
            image = 'https://www.vertivco.com/globalassets/images/on-page-image/612x455/tm-rom-na-612x455-26074-ds-ec-fans-teaser-image_104060_0.jpg';
        $(`<li class="collection-item avatar">    <img src="${image}" alt="" class="circle">    <span class="title">${item.descricao}</span>    <p class="text-small">${item.partNumber}</p><a href="javascript:loadDetail('solucoes/detail','${item.id}')" class="secondary-content"><i class="material-icons">keyboard_arrow_right</i></a></li>`).appendTo($('#lista-solucoes'));
    });
    removeLoadingList(selector);
}


function genPropostaFromSolution() {
    if(validate()) {
        $('#btn-gerar-prop').html('ENVIANDO');
        $('#btn-gerar-prop').attr('disabled', '');
        let modelSolucao = {
            idCliente: $('#idClienteSolucao').val(),
            idSolucao: $('#idSolucao').val(),
            nomeProjeto: $('#nomeProjetoCopia').val(),
            nomeContato: $('#nomeContatoCopia').val(),
            finalidade: $('#finalidadeCopia').val(),
            codPessoaContato: null,
            canalDistribuicao: "21",
            percentualComissao: 10.5,
            status: 1,
            situacao: 1,
            contato: {
                ddiTelefone: "+55",
                dddTelefone: "11",
                telefone: $('#telefoneContatoCopia').val(),
                ddiCelular: null,
                dddCelular: null,
                celular: null,
                email: $('#emailContatoCopia').val()
            }
        };


        let settingsConvert = {
            "async": true,
            "crossDomain": true,
            "url": "https://api-vertiv-biz.mybluemix.net/api/SolutionBox/ConvertToInvoice",
            "method": "POST",
            "headers": {
                "Content-Type": "application/json",
                "authorization": `bearer ${JSON.parse(localStorage.getItem('token')).token}`,
                "Cache-Control": "no-cache"
            },
            "processData": false,
            "data": JSON.stringify(modelSolucao)
        };

        $.ajax(settingsConvert).done(function (response) {

            toast(`A proposta ${response.nProposta} foi gerada com sucesso.`);
            closeMainModal();
        }).fail(function (response, status) {
            $('#btn-gerar-prop').removeAttr('disabled');
            showError(response, status)
        }).always(function () {
            $('#btn-gerar-prop').html('ENVIAR');
            $('#btn-gerar-prop').removeAttr('disabled');
        });
    }
}

function closeSolutionModal() {
    clearModal();
    $('#modalGenCopia').modal('close');
}


function clearModal() {
    $('#idClienteSolucao').val('');
    $('#idSolucao').val('');
    $('#nomeProjetoCopia').val('');
    $('#nomeContatoCopia').val('');
    $('#finalidadeCopia').val('');
    $('#cnpjCliente').val('');
    $('#nomeClienteCopia').val('');
    Materialize.updateTextFields();
    $('select').material_select('destroy');
    $('select').material_select();
}


function validate() {
    let err = [];

    if ($('#cnpjCliente').val() === '' || $('#cnpjCliente').val() === 'undefined' || $('#cnpjCliente').val() === null) {
        err.push("O campo [CNPJ] é obrigatório");
        setInvalid('#cnpjCliente');
    }
    else {
        setValid('#cnpjCliente');
    }
    if ($('#nomeProjetoCopia').val() === '' || $('#nomeProjetoCopia').val() === 'undefined' || $('#nomeProjetoCopia').val() === null) {
        err.push("O campo [Nome do Projeto] é obrigatório");
        setInvalid('#nomeProjetoCopia');
    }
    else {
        setValid('#nomeProjetoCopia');
    }
    if ($('#nomeContatoCopia').val() === '' || $('#nomeContatoCopia').val() === 'undefined' || $('#nomeContatoCopia').val() === null) {
        err.push("O campo [Nome do Contato] é obrigatório");
        setInvalid('#nomeContatoCopia');
    }
    else {
        setValid('#nomeContatoCopia');
    }
    if ($('#nomeClienteCopia').val() === '' || $('#nomeClienteCopia').val() === 'undefined' || $('#nomeClienteCopia').val() === null) {
        err.push("O campo [Nome do Cliente] é obrigatório");
        setInvalid('#nomeClienteCopia');
    }
    else {
        setValid('#nomeClienteCopia');
    }
    if ($('#emailContatoCopia').val() === '' || $('#emailContatoCopia').val() === 'undefined' || $('#emailContatoCopia').val() === null || !isEmail($('#emailContatoCopia').val())) {
        err.push("O campo [Email do Contato] é obrigatório");
        setInvalid('#emailContatoCopia');
    }
    else {
        setValid('#emailContatoCopia');
    }
    if ($('#telefoneContatoCopia').val() === '' || $('#telefoneContatoCopia').val() === 'undefined' || $('#telefoneContatoCopia').val() === null) {
        err.push("O campo [Telefone do Contato] é obrigatório");
        setInvalid('#telefoneContatoCopia');
    }
    else {
        setValid('#telefoneContatoCopia');
    }
    if (err.length > 0) {
        let erros = '';
        $.each(err, function (i, item) {
            erros += `${item}<br/>`
        });
        console.log(erros);
        toast(erros);
    }
    else {
        return true;
    }
}

function setInvalid(element) {
    $(element).addClass('invalid');
}

function setValid(element) {
    $(element).removeClass('invalid');
}


function showError(response, status) {
    toast(response.responseJSON.retorno.error);
}
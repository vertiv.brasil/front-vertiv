let proposta = {};
let base64File = '';

$(document).ready(function () {
    proposta = JSON.parse(sessionStorage.getItem('proposta'));
    $('#file-pedido').on('change', function () {
        const files = document.getElementById('filePedido').files;
        if (files.length > 0) {
            getBase64(files[0]);
        }
    });


    $('.datepicker').pickadate({
        selectMonths: false, // Creates a dropdown to control month
        selectYears: 15, // Creates a dropdown of 15 years to control year,
        today: 'Hoje',
        clear: 'Limpar',
        close: 'Ok',
        monthsFull: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Juno', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        monthsShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
        weekdaysFull: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
        weekdaysShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
        formatSubmit: 'dd/mm/yyyy',
        closeOnSelect: false // Close upon selecting a date,
    });

    if (proposta !== null) {
        loadData();
        Materialize.updateTextFields();
        $('select').material_select();
        $('.modal').modal();
        $('#app-action').html('');
        $('.collapsible').collapsible();
    }
    else loadPage('propostas/index');

    getMenuOpcoes();
});

function loadData() {
    setTitle(`Kikcoff - Proposta ${proposta.nProposta} (${proposta.objSituacao.value})`);

    $('#proposta').val(proposta.nProposta);
    $('#cnpjFat').val(proposta.cnpj);
    $('#inscricaoEstadualFat').val(proposta.inscricaoEstadual);
    $('#razaoSocialFat').val(proposta.nomeCliente);
    if (proposta.enderecoCliente != null) {
        $('#bairroFat').val(proposta.enderecoCliente.bairro);
        $('#cidadeFat').val(proposta.enderecoCliente.cidade);
        $('#cepFat').val(proposta.enderecoCliente.cep);
        $('#estadoFat').val(proposta.enderecoCliente.estado);
        $('#logradouroFat').val(proposta.enderecoCliente.logradouro);
        $('#numeroFat').val(proposta.enderecoCliente.numero);
    }

    $.each(proposta.produtos, function (i, item) {
        var template = `<tr>
            <td>${item.numSap}</td>
            <td>${item.produto.partNumber}</td>
            <td>${item.qtde}</td>
            <td>${item.valorBruto.toLocaleString('pt-BR', {
            minimumFractionDigits: 2,
            style: 'currency',
            currency: 'BRL'
        })}</td>
            <td><input id="dt_${item.numSap}_${item.produto.id}" type="date" value="${new Date().toLocaleDateString()}"></td>
        </tr>`;

        $(template).appendTo($('#table-kickoff'));
    });

    Materialize.updateTextFields();
}

function copyCobFromFat() {

    $('#cnpjCob').val($('#cnpjFat').val());
    $('#razaoSocialCob').val($('#razaoSocialFat').val());
    $('#inscricaoEstadualCob').val($('#inscricaoEstadualFat').val());
    $('#bairroCob').val($('#bairroFat').val());
    $('#celularCob').val($('#celularFat').val());
    $('#cidadeCob').val($('#cidadeFat').val());
    $('#cepCob').val($('#cepFat').val());
    $('#eMailCob').val($('#eMailFat').val());
    $('#estadoCob').val($('#estadoFat').val());
    $('#logradouroCob').val($('#logradouroFat').val());
    $('#nomeCob').val($('#nomeFat').val());
    $('#numeroCob').val($('#numeroFat').val());
    $('#telefoneCob').val($('#telefoneFat').val());
    Materialize.updateTextFields();
}

function copyEntFromFat() {

    $('#cnpjEnt').val($('#cnpjFat').val());
    $('#razaoSocialEnt').val($('#razaoSocialFat').val());
    $('#inscricaoEstadualEnt').val($('#inscricaoEstadualFat').val());
    $('#bairroEnt').val($('#bairroFat').val());
    $('#celularEnt').val($('#celularFat').val());
    $('#cidadeEnt').val($('#cidadeFat').val());
    $('#cepEnt').val($('#cepFat').val());
    $('#eMailEnt').val($('#eMailFat').val());
    $('#estadoEnt').val($('#estadoFat').val());
    $('#logradouroEnt').val($('#logradouroFat').val());
    $('#nomeEnt').val($('#nomeFat').val());
    $('#numeroEnt').val($('#numeroFat').val());
    $('#telefoneEnt').val($('#telefoneFat').val());
    Materialize.updateTextFields();
}

function copyEntFromCob() {

    $('#cnpjEnt').val($('#cnpjCob').val());
    $('#razaoSocialEnt').val($('#razaoSocialCob').val());
    $('#inscricaoEstadualEnt').val($('#inscricaoEstadualCob').val());
    $('#bairroEnt').val($('#bairroCob').val());
    $('#celularEnt').val($('#celularCob').val());
    $('#cidadeEnt').val($('#cidadeCob').val());
    $('#cepEnt').val($('#cepCob').val());
    $('#eMailEnt').val($('#eMailCob').val());
    $('#estadoEnt').val($('#estadoCob').val());
    $('#logradouroEnt').val($('#logradouroCob').val());
    $('#nomeEnt').val($('#nomeCob').val());
    $('#numeroEnt').val($('#numeroCob').val());
    $('#telefoneEnt').val($('#telefoneCob').val());
    Materialize.updateTextFields();

}

function getMenuOpcoes() {
    const mnuDownload = proposta.objSituacao.key <= 2 ? `<li><a id="mnu-opt-download-prop" class="orange-text" target="_blank" href="${proposta.download}" id="btn-download-proposta">DOWNLOAD</a></li>` : '';
    const mnuEmail = proposta.objSituacao.key <= 2 ? `<li><a id="mnu-opt-email-prop" class="orange-text" href="#" onclick="sendPropostaByMail();">EMAIL</a></li>` : '';
    const mnuDetail = `<li><a id="mnu-opt-detail-prop" class="orange-text" href="#" onclick="loadDetail('propostas/detail','${proposta.id}');">DETALHE</a></li>`;

    $('#app-action').html(`<a class="dropdown-button btn orange" href="#" data-activates="dropdown1" style="width: 120px">OPÇÕES<i class="material-icons right">arrow_drop_down</i></a>
    <!-- Dropdown Structure -->
    <ul id="dropdown1" class="dropdown-content">
        ${mnuDownload}
        ${mnuEmail}
        ${mnuDetail}
    </ul>`);


    $('.dropdown-button').dropdown({
            inDuration: 300,
            outDuration: 225,
            constrainWidth: false, // Does not change width of dropdown to that of the activator
            hover: true, // Activate on hover
            gutter: 0, // Spacing from edge
            belowOrigin: false, // Displays dropdown below the button
            alignment: 'left', // Displays dropdown with edge aligned to the left of button
            stopPropagation: false // Stops event propagation
        }
    );

}

function validate() {

    let err = [];

    if ($('#proposta').val() === '' || $('#proposta').val() === 'undefined' || $('#proposta').val() === null) {
        err.push("O campo [Proposta] é obrigatório");
        setInvalid('#proposta');
    }
    else {
        setValid('#proposta');
    }

    if ($('#pedido').val() === '' || $('#pedido').val() === 'undefined' || $('#pedido').val() === null) {
        err.push("O campo [Pedido] é obrigatório");
        setInvalid('#pedido');
    }
    else {
        setValid('#pedido');
    }


    if ($('#dtEmissao').val() === '' || $('#dtEmissao').val() === 'undefined' || $('#dtEmissao').val() === null) {
        err.push("O campo [Data de Emissão do Pedido] é obrigatório");
        setInvalid('#dtEmissao');
    }
    else {
        setValid('#dtEmissao');
    }

    if ($('#cnpjFat').val() === '' || $('#cnpjFat').val() === 'undefined' || $('#cnpjFat').val() === null) {
        err.push("O campo [Faturamento > CNPJ] é obrigatório");
        setInvalid('#cnpjFat');
    }
    else {
        setValid('#cnpjFat');
    }

    if ($('#razaoSocialFat').val() === '' || $('#razaoSocialFat').val() === 'undefined' || $('#razaoSocialFat').val() === null) {
        err.push("O campo [Faturamento > Razão Social] é obrigatório");
        setInvalid('#razaoSocialFat');
    }
    else {
        setValid('#razaoSocialFat');
    }

    if ($('#inscricaoEstadualFat').val() === '' || $('#inscricaoEstadualFat').val() === 'undefined' || $('#inscricaoEstadualFat').val() === null) {
        err.push("O campo [Faturamento > Inscrição Estadual] é obrigatório");
        setInvalid('#inscricaoEstadualFat');
    }
    else {
        setValid('#inscricaoEstadualFat');
    }

    if ($('#inscricaoEstadualFat').val() === '' || $('#inscricaoEstadualFat').val() === 'undefined' || $('#inscricaoEstadualFat').val() === null) {
        err.push("O campo [Faturamento > Inscrição Estadual] é obrigatório");
        setInvalid('#inscricaoEstadualFat');
    }
    else {
        setValid('#inscricaoEstadualFat');
    }

    if ($('#nomeFat').val() === '' || $('#nomeFat').val() === 'undefined' || $('#nomeFat').val() === null) {
        err.push("O campo [Faturamento > Nome] é obrigatório");
        setInvalid('#nomeFat');
    }
    else {
        setValid('#nomeFat');
    }

    if ($('#logradouroFat').val() === '' || $('#logradouroFat').val() === 'undefined' || $('#logradouroFat').val() === null) {
        err.push("O campo [Faturamento > Logradouro] é obrigatório");
        setInvalid('#logradouroFat');
    }
    else {
        setValid('#logradouroFat');
    }

    if ($('#numeroFat').val() === '' || $('#numeroFat').val() === 'undefined' || $('#numeroFat').val() === null) {
        err.push("O campo [Faturamento > Número] é obrigatório");
        setInvalid('#numeroFat');
    }
    else {
        setValid('#numeroFat');
    }

    if ($('#bairroFat').val() === '' || $('#bairroFat').val() === 'undefined' || $('#bairroFat').val() === null) {
        err.push("O campo [Faturamento > Bairro] é obrigatório");
        setInvalid('#bairroFat');
    }
    else {
        setValid('#bairroFat');
    }

    if ($('#cidadeFat').val() === '' || $('#cidadeFat').val() === 'undefined' || $('#cidadeFat').val() === null) {
        err.push("O campo [Faturamento > Cidade] é obrigatório");
        setInvalid('#cidadeFat');
    }
    else {
        setValid('#cidadeFat');
    }

    if ($('#estadoFat').val() === '' || $('#estadoFat').val() === 'undefined' || $('#estadoFat').val() === null) {
        err.push("O campo [Faturamento > Estado] é obrigatório");
        setInvalid('#estadoFat');
    }
    else {
        setValid('#estadoFat');
    }

    if ($('#telefoneFat').val() === '' || $('#telefoneFat').val() === 'undefined' || $('#telefoneFat').val() === null) {
        err.push("O campo [Faturamento > Telefone] é obrigatório");
        setInvalid('#telefoneFat');
    }
    else {
        setValid('#telefoneFat');
    }

    if ($('#celularFat').val() === '' || $('#celularFat').val() === 'undefined' || $('#celularFat').val() === null) {
        err.push("O campo [Faturamento > Celular] é obrigatório");
        setInvalid('#celularFat');
    }
    else {
        setValid('#celularFat');
    }

    if ($('#eMailFat').val() === '' || $('#eMailFat').val() === 'undefined' || $('#eMailFat').val() === null) {
        err.push("O campo [Faturamento > E-mail] é obrigatório");
        setInvalid('#eMailFat');
    }
    else {
        setValid('#eMailFat');
    }


    if ($('#cnpjCob').val() === '' || $('#cnpjCob').val() === 'undefined' || $('#cnpjCob').val() === null) {
        err.push("O campo [Cobrança > CNPJ] é obrigatório");
        setInvalid('#cnpjCob');
    }
    else {
        setValid('#cnpjCob');
    }

    if ($('#razaoSocialCob').val() === '' || $('#razaoSocialCob').val() === 'undefined' || $('#razaoSocialCob').val() === null) {
        err.push("O campo [Cobrança > Razão Social] é obrigatório");
        setInvalid('#razaoSocialCob');
    }
    else {
        setValid('#razaoSocialCob');
    }

    if ($('#inscricaoEstadualCob').val() === '' || $('#inscricaoEstadualCob').val() === 'undefined' || $('#inscricaoEstadualCob').val() === null) {
        err.push("O campo [Cobrança > Inscrição Estadual] é obrigatório");
        setInvalid('#inscricaoEstadualCob');
    }
    else {
        setValid('#inscricaoEstadualCob');
    }

    if ($('#inscricaoEstadualCob').val() === '' || $('#inscricaoEstadualCob').val() === 'undefined' || $('#inscricaoEstadualCob').val() === null) {
        err.push("O campo [Cobrança > Inscrição Estadual] é obrigatório");
        setInvalid('#inscricaoEstadualCob');
    }
    else {
        setValid('#inscricaoEstadualCob');
    }

    if ($('#nomeCob').val() === '' || $('#nomeCob').val() === 'undefined' || $('#nomeCob').val() === null) {
        err.push("O campo [Cobrança > Nome] é obrigatório");
        setInvalid('#nomeCob');
    }
    else {
        setValid('#nomeCob');
    }

    if ($('#logradouroCob').val() === '' || $('#logradouroCob').val() === 'undefined' || $('#logradouroCob').val() === null) {
        err.push("O campo [Cobrança > Logradouro] é obrigatório");
        setInvalid('#logradouroCob');
    }
    else {
        setValid('#logradouroCob');
    }

    if ($('#numeroCob').val() === '' || $('#numeroCob').val() === 'undefined' || $('#numeroCob').val() === null) {
        err.push("O campo [Cobrança > Número] é obrigatório");
        setInvalid('#numeroCob');
    }
    else {
        setValid('#numeroCob');
    }

    if ($('#bairroCob').val() === '' || $('#bairroCob').val() === 'undefined' || $('#bairroCob').val() === null) {
        err.push("O campo [Cobrança > Bairro] é obrigatório");
        setInvalid('#bairroCob');
    }
    else {
        setValid('#bairroCob');
    }

    if ($('#cidadeCob').val() === '' || $('#cidadeCob').val() === 'undefined' || $('#cidadeCob').val() === null) {
        err.push("O campo [Cobrança > Cidade] é obrigatório");
        setInvalid('#cidadeCob');
    }
    else {
        setValid('#cidadeCob');
    }

    if ($('#estadoCob').val() === '' || $('#estadoCob').val() === 'undefined' || $('#estadoCob').val() === null) {
        err.push("O campo [Cobrança > Estado] é obrigatório");
        setInvalid('#estadoCob');
    }
    else {
        setValid('#estadoCob');
    }

    if ($('#telefoneCob').val() === '' || $('#telefoneCob').val() === 'undefined' || $('#telefoneCob').val() === null) {
        err.push("O campo [Cobrança > Telefone] é obrigatório");
        setInvalid('#telefoneCob');
    }
    else {
        setValid('#telefoneCob');
    }

    if ($('#celularCob').val() === '' || $('#celularCob').val() === 'undefined' || $('#celularCob').val() === null) {
        err.push("O campo [Cobrança > Celular] é obrigatório");
        setInvalid('#celularCob');
    }
    else {
        setValid('#celularCob');
    }

    if ($('#eMailCob').val() === '' || $('#eMailCob').val() === 'undefined' || $('#eMailCob').val() === null) {
        err.push("O campo [Cobrança > E-mail] é obrigatório");
        setInvalid('#eMailCob');
    }
    else {
        setValid('#eMailCob');
    }

    if ($('#cnpjEnt').val() === '' || $('#cnpjEnt').val() === 'undefined' || $('#cnpjEnt').val() === null) {
        err.push("O campo [Entrega > CNPJ] é obrigatório");
        setInvalid('#cnpjEnt');
    }
    else {
        setValid('#cnpjEnt');
    }

    if ($('#razaoSocialEnt').val() === '' || $('#razaoSocialEnt').val() === 'undefined' || $('#razaoSocialEnt').val() === null) {
        err.push("O campo [Entrega > Razão Social] é obrigatório");
        setInvalid('#razaoSocialEnt');
    }
    else {
        setValid('#razaoSocialEnt');
    }

    if ($('#inscricaoEstadualEnt').val() === '' || $('#inscricaoEstadualEnt').val() === 'undefined' || $('#inscricaoEstadualEnt').val() === null) {
        err.push("O campo [Entrega > Inscrição Estadual] é obrigatório");
        setInvalid('#inscricaoEstadualEnt');
    }
    else {
        setValid('#inscricaoEstadualEnt');
    }

    if ($('#inscricaoEstadualEnt').val() === '' || $('#inscricaoEstadualEnt').val() === 'undefined' || $('#inscricaoEstadualEnt').val() === null) {
        err.push("O campo [Entrega > Inscrição Estadual] é obrigatório");
        setInvalid('#inscricaoEstadualEnt');
    }
    else {
        setValid('#inscricaoEstadualEnt');
    }

    if ($('#nomeEnt').val() === '' || $('#nomeEnt').val() === 'undefined' || $('#nomeEnt').val() === null) {
        err.push("O campo [Entrega > Nome] é obrigatório");
        setInvalid('#nomeEnt');
    }
    else {
        setValid('#nomeEnt');
    }

    if ($('#logradouroEnt').val() === '' || $('#logradouroEnt').val() === 'undefined' || $('#logradouroEnt').val() === null) {
        err.push("O campo [Entrega > Logradouro] é obrigatório");
        setInvalid('#logradouroEnt');
    }
    else {
        setValid('#logradouroEnt');
    }

    if ($('#numeroEnt').val() === '' || $('#numeroEnt').val() === 'undefined' || $('#numeroEnt').val() === null) {
        err.push("O campo [Entrega > Número] é obrigatório");
        setInvalid('#numeroEnt');
    }
    else {
        setValid('#numeroEnt');
    }

    if ($('#bairroEnt').val() === '' || $('#bairroEnt').val() === 'undefined' || $('#bairroEnt').val() === null) {
        err.push("O campo [Entrega > Bairro] é obrigatório");
        setInvalid('#bairroEnt');
    }
    else {
        setValid('#bairroEnt');
    }

    if ($('#cidadeEnt').val() === '' || $('#cidadeEnt').val() === 'undefined' || $('#cidadeEnt').val() === null) {
        err.push("O campo [Entrega > Cidade] é obrigatório");
        setInvalid('#cidadeEnt');
    }
    else {
        setValid('#cidadeEnt');
    }

    if ($('#estadoEnt').val() === '' || $('#estadoEnt').val() === 'undefined' || $('#estadoEnt').val() === null) {
        err.push("O campo [Entrega > Estado] é obrigatório");
        setInvalid('#estadoEnt');
    }
    else {
        setValid('#estadoEnt');
    }

    if ($('#telefoneEnt').val() === '' || $('#telefoneEnt').val() === 'undefined' || $('#telefoneEnt').val() === null) {
        err.push("O campo [Entrega > Telefone] é obrigatório");
        setInvalid('#telefoneEnt');
    }
    else {
        setValid('#telefoneEnt');
    }

    if ($('#celularEnt').val() === '' || $('#celularEnt').val() === 'undefined' || $('#celularEnt').val() === null) {
        err.push("O campo [Entrega > Celular] é obrigatório");
        setInvalid('#celularEnt');
    }
    else {
        setValid('#celularEnt');
    }

    if ($('#eMailEnt').val() === '' || $('#eMailEnt').val() === 'undefined' || $('#eMailEnt').val() === null) {
        err.push("O campo [Entrega > E-mail] é obrigatório");
        setInvalid('#eMailEnt');
    }
    else {
        setValid('#eMailEnt');
    }

    if ($('#file-pedido').val() === '' || $('#file-pedido').val() === 'undefined' || $('#file-pedido').val() === null) {
        err.push("O campo [Anexo] é obrigatório");
        setInvalid('#file-pedido');
    }
    else {
        setValid('#file-pedido');
    }


    if (err.length > 0) {
        let erros = '';
        $.each(err, function (i, item) {
            erros += `${item}<br/>`
        });
        console.log(erros);
        toast(erros);
    }
    else {
        return true;
    }
}

function sendKickoff() {
    const btn = '#btn-send-kickoff';
    if (validate()) {
        $(btn).text('SALVANDO');
        $(btn).attr('disabled', 'disabled');
        const settings = {
            "async": true,
            "crossDomain": true,
            "url": "https://api-vertiv-biz.mybluemix.net/api/proposta/kickoff",
            //"url": "http://localhost:5000/api/proposta/kickoff",
            "method": "POST",
            "headers": {
                "Token": JSON.parse(localStorage.getItem('token')).token,
                "authorization": `bearer ${JSON.parse(localStorage.getItem('token')).token}`,
                "Content-Type": "application/json",
                "Cache-Control": "no-cache",
            },
            "processData": false,
            "data": {}
        };

        let kickOff = {

                nProposta: $('#proposta').val(),
                nPedido: $('#pedido').val(),
                fatCnpj:
                    $('#cnpjFat').val(),
                fatRazao:
                    $('#razaoSocialFat').val(),
                fatIe:
                    $('#inscricaoEstadualFat').val(),
                fatEndereco:
                    $('#logradouroFat').val(),
                fatNumero:
                    $('#numeroFat').val(),
                fatBairro:
                    $('#bairroFat').val(),
                fatCidade:
                    $('#cidadeFat').val(),
                fatUf:
                    $('#estadoFat').val(),
                fatCompl:
                    $('#complementoFat').val(),
                fatCep:
                    $('#cepFat').val(),

                fatPessoa:
                    $('#nomeFat').val(),
                email:
                    $('#eMailFat').val(),
                fatEmail:
                    $('#eMailFat').val(),
                fatDddTel:
                    $('#telefoneFat').val().split(' ')[0].replace('(', '').replace(')', ''),
                fatTelefone:
                    $('#telefoneFat').val().split(' ')[1],
                fatCelDdd:
                    $('#celularFat').val().split(' ')[0].replace('(', '').replace(')', ''),
                fatCelular:
                    $('#celularFat').val().split(' ')[1],

                cobCnpj:
                    $('#cnpjCob').val(),
                cobRazao:
                    $('#razaoSocialCob').val(),
                cobIe:
                    $('#inscricaoEstadualCob').val(),
                cobEndereco:
                    $('#logradouroCob').val(),
                cobNumero:
                    $('#numeroCob').val(),
                cobBairro:
                    $('#bairroCob').val(),
                cobCidade:
                    $('#cidadeCob').val(),
                cobUf:
                    $('#estadoCob').val(),
                cobCompl:
                    $('#complementoCob').val(),
                cobCep:
                    $('#cepCob').val(),

                cobPessoa:
                    $('#nomeCob').val(),
                cobEmail:
                    $('#eMailCob').val(),
                cobDddTel:
                    $('#telefoneCob').val().split(' ')[0].replace('(', '').replace(')', ''),
                cobTelefone:
                    $('#telefoneCob').val().split(' ')[1],
                cobCelDdd:
                    $('#celularCob').val().split(' ')[0].replace('(', '').replace(')', ''),
                cobCelular:
                    $('#celularCob').val().split(' ')[1],


                entCnpj:
                    $('#cnpjEnt').val(),
                entRazao:
                    $('#razaoSocialEnt').val(),
                entIe:
                    $('#inscricaoEstadualEnt').val(),
                entEndereco:
                    $('#logradouroEnt').val(),
                entNumero:
                    $('#numeroEnt').val(),
                entBairro:
                    $('#bairroEnt').val(),
                entCidade:
                    $('#cidadeEnt').val(),
                entUf:
                    $('#estadoEnt').val(),
                entCompl:
                    $('#complementoEnt').val(),
                entCep:
                    $('#cepEnt').val(),

                entPessoa:
                    $('#nomeEnt').val(),
                entEmail:
                    $('#eMailEnt').val(),
                entDddTel:
                    $('#telefoneEnt').val().split(' ')[0].replace('(', '').replace(')', ''),
                entTelefone:
                    $('#telefoneEnt').val().split(' ')[1],
                entCelDdd:
                    $('#celularEnt').val().split(' ')[0].replace('(', '').replace(')', ''),
                entCelular:
                    $('#celularEnt').val().split(' ')[1],

                dtEmissaoPedCli:
                    new Date($('#dtEmissao').val()).toLocaleDateString(),
                dtRecebimPedCli:
                    new Date().toLocaleDateString(),

                observacoes:
                    $('#observacoes').val(),
                ressalvas:
                    $('#ressalvas').val(),

                aceitaEntregaParcial:
                    $('#aceitaEntregaParcial').prop('checked') ? 'Sim' : 'Não',
                poNecessitaAssin:
                    $('#necessitaAssinatura').prop('checked') ? 'Sim' : 'Não',
                poPossuiTermCond:
                    $('#possuiTermo').prop('checked') ? 'Sim' : 'Não',

                produtos:
                    [],

                fileName:
                    $('#file-pedido').val(),
                urlPedido:
                base64File

            }
        ;

        $.each(proposta.produtos, function (i, item) {
            var id = `#dt_${item.numSap}_${item.produto.id}`;
            kickOff.produtos.push(
                {
                    itemSap: item.numSap,
                    dataEntrega: new Date($(id).val()).toLocaleDateString()
                }
            );
        });

        try {

            toast(`Favor aguardar enquanto a resquisição está sendo processada.`);
            settings.data = JSON.stringify(kickOff);
            $.ajax(settings).done(function (response) {

                console.log(response);
                $(btn).text('SALVAR');
                $(btn).removeAttr('disabled');
                toast(`Proposta enviada para Kickoff`);
                if ($(document).width() > 600) loadPage('propostas/table');
                else loadPage('propostas/index');
            }).fail(function (response, status) {
                toast(response.responseJSON.retorno.error);
                $(btn).text('SALVAR');
                $(btn).removeAttr('disabled');
            });
        }
        catch (e) {
            $(btn).text('SALVAR');
            $(btn).removeAttr('disabled');
            console.log(e);
            toast('Falha ao enviar a Proposta para Kickoff');
        }
    }

}

function loadKickoff(data) {
    $('#proposta').val(data.nProposta);
    $('#pedido').val(data.nPedido);
    $('#cnpjFat').val(data.fatCnpj);
    $('#razaoSocialFat').val(data.fatRazao);
    $('#inscricaoEstadualFat').val(data.fatIe);
    $('#logradouroFat').val(data.fatEndereco);
    $('#numeroFat').val(data.fatNumero);
    $('#bairroFat').val(data.fatBairro);
    $('#cidadeFat').val(data.fatCidade);
    $('#estadoFat').val(data.fatUf);
    $('#complementoFat').val(data.fatCompl);
    $('#cepFat').val(data.fatCep);
    $('#nomeFat').val(data.fatPessoa);
    $('#eMailFat').val(data.fatEmail);
    $('#telefoneFat').val(data.fatDddTel + data.fatTelefone);
    $('#celularFat').val(data.fatCelDdd + data.fatCelular);
    $('#cnpjCob').val(data.cobCnpj);
    $('#razaoSocialCob').val(data.cobRazao);
    $('#inscricaoEstadualCob').val(data.cobIe);
    $('#logradouroCob').val(data.cobEndereco);
    $('#numeroCob').val(data.cobNumero);
    $('#bairroCob').val(data.cobBairro);
    $('#cidadeCob').val(data.cobCidade);
    $('#estadoCob').val(data.cobUf);
    $('#complementoCob').val(data.cobCompl);
    $('#cepCob').val(data.cobCep);
    $('#nomeCob').val(data.cobPessoa);
    $('#eMailCob').val(data.cobEmail);
    $('#telefoneCob').val(data.cobDddTel + data.cobTelefone);
    $('#celularCob').val(data.cobCelDdd + data.cobCelular);
    $('#cnpjEnt').val(data.entCnpj);
    $('#razaoSocialEnt').val(data.entRazao);
    $('#inscricaoEstadualEnt').val(data.entIe);
    $('#logradouroEnt').val(data.entEndereco);
    $('#numeroEnt').val(data.entNumero);
    $('#bairroEnt').val(data.entBairro);
    $('#cidadeEnt').val(data.entCidade);
    $('#estadoEnt').val(data.entUf);
    $('#complementoEnt').val(data.entCompl);
    $('#cepEnt').val(data.entCep);
    $('#nomeEnt').val(data.entPessoa);
    $('#eMailEnt').val(data.entEmail);
    $('#telefoneEnt').val(data.entDddTel + data.entTelefone);
    $('#celularEnt').val(data.entCelDdd + data.entCelular);
    $('#dtEmissao').val(data.dtEmissaoPedCli);
    $('#observacoes').val(data.observacoes);
    $('#ressalvas').val(data.ressalvas);
    $('#aceitaEntregaParcial').prop('checked', data.aceitaEntregaParcial === 'Sim');
    $('#necessitaAssinatura').prop('checked', data.poNecessitaAssin === 'Sim');
    $('#possuiTermo').prop('checked', data.poPossuiTermCond === 'Sim');

    $(input).attr('disabled');
}

function sendPropostaByMail() {
    const settings = {
        "async": true,
        "crossDomain": true,
        "url": "https://api-vertiv-biz.mybluemix.net/api/proposta/sendmail",
        "method": "POST",
        "headers": {
            "Token": JSON.parse(localStorage.getItem('token')).token,
            "authorization": `bearer ${JSON.parse(localStorage.getItem('token')).token}`,
            "Content-Type": "application/json",
            "Cache-Control": "no-cache"
        },
        "processData": false,
        "data": JSON.stringify({
            nPoposta: proposta.nProposta,
            email: JSON.parse(localStorage.getItem('user')).email,
            fileUrl: proposta.download
        })
    };

    $.ajax(settings).done(function (response) {
        toast(`Proposta enviada com sucesso para o e-mail ${JSON.parse(localStorage.getItem('user')).email}.`);
    }).fail(function (response, status) {
        toast(`Falha ao enviar a proposta por e-mail ${JSON.parse(localStorage.getItem('user')).email}.`);
    });
}

function setInvalid(element) {
    $(element).addClass('invalid');
}

function setValid(element) {
    $(element).removeClass('invalid');
}

function getBase64(file) {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
        base64File = reader.result.split(',')[1];
        console.log(base64File);
    };
    reader.onerror = function (error) {
        console.log('Error: ', error);
    };
}

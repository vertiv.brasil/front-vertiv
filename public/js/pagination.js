let funcCallback;
let funcErrorCallcack;
let numPages = 1;
let currentPage = 1;
let total = 1;
let tableElement;

function getServerPagination(element, table, apiPAGINA, totalItems, itemsPerPage, callback, errorCallback) {
    tableElement = table;
    total = totalItems;
    funcCallback = callback;
    funcErrorCallcack = errorCallback;
    numPages = Math.floor(totalItems / itemsPerPage);
    if (totalItems % itemsPerPage > 0) numPages++;

    const initRec = 1 + ((currentPage * itemsPerPage) - itemsPerPage);
    let fimRec = (initRec + itemsPerPage - 1);

    if(fimRec > totalItems) fimRec = totalItems;

    let pages = `<ul id="page-status" class="pagination left"><li>Exibindo de ${initRec} até ${fimRec} de ${totalItems} registro(s)</li></ul>`;

    pages += '<ul class="pagination right">\n' +
        '    <li id="first-page" class="disabled"><a href="#" onclick="decreasePage()"><i class="material-icons">chevron_left</i></a></li>\n' +
        '    <li>Página</li>\n' +
        '    <li><select id="pages">';

    for (let index = 1; index <= numPages; index++) {
        pages += `<option value="${index}">${index}</option>`;
    }

    pages += `</select></li>
    <li>de <span id="totalpages">${numPages}</span></div></li>
    <li id="last-page" class="waves-effect"><a href="#" onclick="increasePage()"><i class="material-icons">chevron_right</i></a></li>
</ul>`;

    element.html(pages);
}

function paginate(apiPAGINA, itemsPerPage) {
    loadingTable(tableElement);

    const page = $('#pages').val();
    currentPage = page;
    const initRec = 1 + ((currentPage * itemsPerPage) - itemsPerPage);
    let fimRec = (initRec + itemsPerPage - 1);

    if(fimRec > total) fimRec = total;
    const sts = `<li>Exibindo de ${initRec} até ${fimRec} de ${total} registro(s)</li>`;

    $('#page-status').html(sts);

    if (page === 1) {
        $('#first-page').removeClass('waves-effect');
        $('#first-page').addClass('disabled');
    }
    else {
        $('#first-page').removeClass('disabled');
        $('#first-page').addClass('waves-effect');
    }
    if (page === numPages) {
        $('#last-page').removeClass('waves-effect');
        $('#last-page').addClass('disabled');
    }
    else {
        $('#last-page').removeClass('disabled');
        $('#last-page').addClass('waves-effect');
    }

    const settings = {
        "async": true,
        "crossDomain": true,
        "url": `${apiPAGINA}?ppage=${page}&pquantidade=${itemsPerPage}&filter=${sessionStorage.getItem('filter')}`,
        "method": "GET",
        "headers": {
            "Cache-Control": "no-cache",
            "authorization": `bearer ${JSON.parse(localStorage.getItem('token')).token}`,
            "Postman-Token": "6b6f0b3f-dbf9-c1a8-5c21-f0cfc2f72618"
        }
    };

    $.ajax(settings).done(
        function (data) {
            funcCallback(data);
        }
    ).fail(function (response, status) {
        funcErrorCallcack(response, status);
    });
}

function increasePage() {
    if (currentPage < numPages) {
        currentPage++;
        $('#pages').val(currentPage).change();
        $('#pages').material_select('destroy');
        $('#pages').material_select();

    }
}

function decreasePage() {
    if (currentPage > 1) {
        currentPage--;
        $('#pages').val(currentPage).change();
        $('#pages').material_select('destroy');
        $('#pages').material_select();
    }
}

function getColumnCount() {
    let columnCount = 0;
    $('table:first tr:first > th').each(function() {
        const colspanValue = $(this).attr('colspan');
        if (colspanValue === undefined) {
            columnCount++;
        } else {
            columnCount = columnCount + parseInt(colspanValue);
        }
    });
    return columnCount;
}

function loadingTable(element) {
    $.get('views/loader/spinner.html').done(function (result) {
        element.html(`<tr><td colspan="${getColumnCount()}">${result}</td></tr>`);
    });
}

function loadingList(element) {
    $.get('views/loader/spinner.html').done(function (result) {
        $(`<li id="loading-list">${result}</li>`).appendTo($(element));
    });
}

function removeLoadingList(element) {
    if ($('#loading-list').length) {
        $('#loading-list').remove();
    }
}


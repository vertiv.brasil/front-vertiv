const apiCliente = 'https://api-vertiv-biz.mybluemix.net/api/cliente/mobile';

const settings = {
    "async": true,
    "crossDomain": true,
    "url": `${apiCliente}?ppage=1&pquantidade=10`,
    "method": "GET",
    "headers": {
        "Cache-Control": "no-cache",
        "authorization": `bearer ${JSON.parse(localStorage.getItem('token')).token}`,
        "Postman-Token": "6b6f0b3f-dbf9-c1a8-5c21-f0cfc2f72618"
    }
};

$(document).ready(function () {


    $('#app-action').html('<a href="javascript:loadPage(\'clientes/add\')" class="btn waves-effect waves-light orange right">+ NOVO</a>');

    setTitle('Clientes');

    loadingTable($('#clientes'));

    loadDataFromServer('');

});

function populateTable(response) {
    let rows = '';
    $.each(response.cliente, function (i, item) {
        rows += `<tr><td class="cnpj">${item.cnpj}</td><td>${item.nome}</td><td>${item.nomeFantasia}</td><td>${item.cidade}</td><td>${item.estado}</td><td class="center-align"><a href="#" class="btn btn-default waves-effect" onclick="loadDetail('clientes/detail','${item.id}')"><i class="material-icons">visibility</i></a></td></tr>`;
    });
    $('#clientes').html(rows);
}

function filterTable() {
    loadingTable($('#clientes'));
    const filter = $('#filter-input').val();
    sessionStorage.setItem('filter', filter);
    loadDataFromServer(filter);
}

function loadDataFromServer(filter) {
    settings.url = `${settings.url}&filter=${filter}`;

    $.ajax(settings).done(function (response) {
        populateTable(response);
        getServerPagination($('#pagnination'), $('#clientes'), apiCliente, response.quantidade, 10, populateTable, showError);
        Materialize.updateTextFields();
        $('#pages').on('change', function () {
            paginate(apiCliente, 10);
        }).material_select();
    });
    settings.url = `${apiCliente}?ppage=1&pquantidade=10`
}

function showError(response, status) {
    toast('Falha ao buscar dados no servidor');
}
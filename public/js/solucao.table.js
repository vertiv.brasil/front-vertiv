const api = 'https://api-vertiv-biz.mybluemix.net/api/SolutionBox/mobile';
const settings = {
    "async": true,
    "crossDomain": true,
    "url": `${api}?ppage=1&pquantidade=10`,
    "method": "GET",
    "headers": {
        "Cache-Control": "no-cache",
        "authorization": `bearer ${JSON.parse(localStorage.getItem('token')).token}`,
        "Postman-Token": "6b6f0b3f-dbf9-c1a8-5c21-f0cfc2f72618"
    }
};

$(document).ready(function () {

    $('#app-action').html('');
    setTitle('Soluções');


    loadingTable( $('#solucoes'));

    loadDataFromServer('');

});


function filterTable() {
    loadingTable($('#solucoes'));
    const filter = $('#filter-input').val();
    sessionStorage.setItem('filter', filter);
    loadDataFromServer(filter);
}

function loadDataFromServer(filter) {
    settings.url = `${settings.url}&filter=${filter}`;

    $.ajax(settings).done(function (response) {
        populateTable(response);
        getServerPagination($('#pagnination'),$('#solucoes'), api, response.quantidade, 10, populateTable, showError);
        Materialize.updateTextFields();
        $('#pages').on('change', function () {
            paginate(api, 10);
        }).material_select();
    });

    settings.url = `${api}?ppage=1&pquantidade=10`
}

function populateTable(response) {
    let rows = '';
    $.each(response.solutionBoxList, function (i, item) {
        rows += `<tr><td>${item.partNumber}</td><td>${new Date(item.dataCriacao).toLocaleDateString('pt-BR')} - ${new Date(item.dataCriacao).toLocaleTimeString('pt-BR', {hour12: false})}</td><td>${item.descricao}</td><td>${new Date(item.dataInicial).toLocaleDateString('pt-BR')}</td><td>${item.objVisibilidade.value}</td><td class="center-align"><a href="#" class="btn btn-default waves-effect" onclick="loadDetail('solucoes/detail','${item.id}')"><i class="material-icons">visibility</i></a></td></tr>`;
    });
    $('#solucoes').html(rows);
}

function showError(response, status) {
    toast('Falha ao buscar dados no servidor');
}
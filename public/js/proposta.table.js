const api = 'https://api-vertiv-biz.mybluemix.net/api/proposta/mobile';
const settings = {
    "async": true,
    "crossDomain": true,
    "url": `${api}?ppage=1&pquantidade=10`,
    "method": "GET",
    "headers": {
        "Cache-Control": "no-cache",
        "authorization": `bearer ${JSON.parse(localStorage.getItem('token')).token}`,
        "Postman-Token": "6b6f0b3f-dbf9-c1a8-5c21-f0cfc2f72618"
    }
};

$(document).ready(function () {

    $('#app-action').html('');
    setTitle('Propostas');


    loadingTable( $('#propostas'));

    loadDataFromServer('');

});


function filterTable() {
    loadingTable($('#propostas'));
    const filter = $('#filter-input').val();
    sessionStorage.setItem('filter', filter);
    loadDataFromServer(filter);
}

function loadDataFromServer(filter) {
    settings.url = `${settings.url}&filter=${filter}`;

    $.ajax(settings).done(function (response) {
        populateTable(response);
        getServerPagination($('#pagnination'), $('#propostas'), api, response.quantidade, 10, populateTable, showError);
        Materialize.updateTextFields();
        $('#pages').on('change', function () {
            paginate(api, 10);
        }).material_select();
    });
    settings.url = `${api}?ppage=1&pquantidade=10`
}

function populateTable(response) {
    let rows = '';
    $.each(response.propostas, function (i, item) {
        rows += `<tr><td>${item.nProposta}</td><td>${new Date(item.dataCriacao).toLocaleDateString('pt-BR')} - ${new Date(item.dataCriacao).toLocaleTimeString('pt-BR', {hour12: false})}</td><td>${item.nomeProjeto}</td><td class="cnpj">${item.cnpj}</td><td>${item.nomeCliente}</td><td>${item.objSituacao.value}</td><td>${item.objStatus.value}</td><td class="money2">${item.valor}</td><td class="center-align"><a href="#" class="btn btn-default waves-effect" onclick="loadDetail('propostas/detail','${item.id}')"><i class="material-icons">visibility</i></a></td></tr>`;
    });
    $('#propostas').html(rows);
}

function showError(response, status) {
    toat('Falha ao buscar dados no servidor');
}
$(document).ready(function () {
    localStorage.clear();
});

function login() {
    const settings = {
        "async": true,
        "crossDomain": true,
        "url": "https://api-vertiv-biz.mybluemix.net/account/login",
        "method": "POST",
        "headers": {
            "Content-Type": "application/json",
            "Cache-Control": "no-cache",
            "Postman-Token": "6c35c9dd-9b71-112b-88d9-0b522112acd4"
        },
        "processData": false,
        "data": `{
  "Email":"${$('#username').val()}",
  "Password":"${$('#password').val()}"}`
    };

    $.ajax(settings).done(function (response) {
        let mili = new Date().getTime();
        mili = mili + (24 * 60 * 1000);
        localStorage.setItem('token', JSON.stringify({token: response.token, issued: mili}));
        localStorage.setItem('user', JSON.stringify(response.usuario));
        window.location.href = 'index.html';
    }).fail(function (response, status) {
        console.log(response);
        switch (response.status) {
            case 500:
                Materialize.toast('Falha no servidor.', 4000);
                break;
            case 400:
                Materialize.toast('Dados inválidos.', 4000);
                break;
            default:
                Materialize.toast('Usuário ou Senha inválidos.', 4000);
                break;

        }
    });
}
const apiProduto = "https://api-vertiv-biz.mybluemix.net/api/Produto/mobile";
let selector  = null;
const settingsListProduto = {
    "async": true,
    "crossDomain": true,
    "url": `${apiProduto}?ppage=1&pquantidade=20`,
    "method": "GET",
    "headers": {
        "Cache-Control": "no-cache",
        "authorization": `bearer ${JSON.parse(localStorage.getItem('token')).token}`
    }
};

$(document).ready(function() {
    selector =  '#lista-produtos';

    $('#app-action').html('');

    setTitle('Produtos');
    $.ajax(settingsListProduto).done(function (response) {
        //loadingList(selector);
        populateList(response);
    });
});


function filterList() {
    const filter = $('#filter-input').val();
    sessionStorage.setItem('filter', filter);
    $(selector).html('');

    loadDataFromServer(filter);
}

function loadDataFromServer(filter) {

    //loadingList(selector);
    settingsListProduto.url = `${settingsListProduto.url}&filter=${filter}`;
    $.ajax(settingsListProduto).done(function (response) {
        populateList(response);
        //getServerListPagination($('#pagnination'), $('#produtos'), api, response.quantidade, 10, populateTable, showError);
        Materialize.updateTextFields();
        //$('#pages').on('change', function () {
        //    paginate(api, 20);
        //}).material_select();
    });
    settingsListProduto.url = `${apiProduto}?ppage=1&pquantidade=20`
}

function populateList(response) {
    $.each(response.produtos, function (i, item) {
        let image = item.imageUrl;
        if(image === null || image === '' || image === 'undefined')
            image='https://www.vertivco.com/globalassets/images/on-page-image/612x455/tm-rom-na-612x455-26074-ds-ec-fans-teaser-image_104060_0.jpg';
        $(`<li class="collection-item avatar">    <img src="${image}" alt="" class="circle">    <span class="title">${item.partNumber}</span>    <p>${item.descricao}<br>${parseFloat(item.valor).toLocaleString('pt-BR',  { minimumFractionDigits: 2 , style: 'currency', currency: 'BRL' })}</p><a href="javascript:loadDetail('produtos/detail','${item.id}')" class="secondary-content"><i class="material-icons">keyboard_arrow_right</i></a></li>`).appendTo($('#lista-produtos'));
    });
    removeLoadingList(selector);
}

function showError(response, status) {
    toast('Falha ao buscar dados no servidor');
}




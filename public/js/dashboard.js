const ctx = $("#myChart");
const ctx2 = $("#myChart2");
const collorPallet = [
    'rgba(242,103,31,1)',
    'rgba(201,27,38,1)',
    'rgba(156,15,95,1)',
    'rgba(22,10,71,1)',
    'rgba(76,0,118,1)'];
const collorPallet2 = [
    'rgba(242,103,31,0.7)',
    'rgba(201,27,38,0.7)',
    'rgba(156,15,95,0.7)',
    'rgba(22,10,71,0.7)',
    'rgba(76,0,118,0.7)'];
const data = {datasets: [{data: [], backgroundColor: []}], labels: []};
const data2 = {datasets: [{data: [], backgroundColor: []}], labels: []};
let myChart = {};
let myChart2 = {};

const revenda = JSON.parse(localStorage.getItem('user')).revendas[0].cnpj;


const settings = {
    "async": true,
    "crossDomain": true,
    "url": `https://api-vertiv-biz.mybluemix.net/api/application/dashboard/${revenda}`,
    "method": "GET",
    "headers": {
        "Content-Type": "application/json",
        "Cache-Control": "no-cache",
        "authorization": `bearer ${JSON.parse(localStorage.getItem('token')).token}`,
        "Postman-Token": "05e058f6-2165-025d-5347-1ec38a2f40ca"
    }
};

$.ajax(settings).done(function (response) {
    let index = 0;
    $.each(response.dashboard.vendasPorProduto, function (i,item) {
        data.datasets[0].data.push(item.value);
        data.datasets[0].backgroundColor.push(collorPallet[index]);
        data.labels.push(item.title);
        index++
    });

    index = 0;
    $.each(response.dashboard.pipeline, function (i,item) {
        data2.datasets[0].data.push(item.value);
        data2.datasets[0].backgroundColor.push(collorPallet2[index]);
        data2.labels.push(item.title);
        index++
    });

    myChart = new Chart(ctx, {
        type: 'doughnut',
        data: data,
        options: {
            responsive: true,
            legend: {
                display: false,
                position: 'top',
            },
            title: {
                display: false,
                text: 'Vendas no mês'
            },
            animation: {
                animateScale: true,
                animateRotate: true
            }
        }
    });

    myChart2 = new Chart(ctx2, {
        type: 'polarArea',
        data: data2,
        options: {
            responsive: true,
            legend: {
                display: false,
                position: 'top',
            },
            title: {
                display: false,
                text: 'Pipeline'
            },
            animation: {
                animateScale: true,
                animateRotate: true
            }
        }
    });

    $('#titleConversao').html(`${(response.dashboard.conversao.percentualValue * 100).toFixed(0)}%`);
    $('#valueConversao').attr('style',`width:${response.dashboard.conversao.percentualValue * 100}%;`);

    $('#titleVendas').html((response.dashboard.vendasRealizadas.value).replace(',','*').replace('.', ',').replace('*','.'));
    $('#valueVendas').attr('style',`width:${response.dashboard.vendasRealizadas.percentualValue * 100}%;`);

    if($('.view').width() > 720) {
        $('#dash-conversao').height($('#dash-pipeline').height());
        $('#dash-vendas').height($('#dash-pipeline').height());
    }
});



$(document).ready(function() {

    $('#app-action').html('');
    setTitle('Dashboard');
    if ($('#btn-bot-mobile').is(":visible"))
        $('#tgt-mobile').tapTarget('open');
});

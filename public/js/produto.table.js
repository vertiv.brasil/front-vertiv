const api = 'https://api-vertiv-biz.mybluemix.net/api/produto/mobile';
const settings = {
    "async": true,
    "crossDomain": true,
    "url": `${api}?ppage=1&pquantidade=10`,
    "method": "GET",
    "headers": {
        "Cache-Control": "no-cache",
        "authorization": `bearer ${JSON.parse(localStorage.getItem('token')).token}`,
    }
};

$(document).ready(function () {

    $('#app-action').html('');
    setTitle('Produtos');

    loadingTable( $('#produtos'));

    loadDataFromServer('');

});

function filterTable() {
    loadingTable($('#produtos'));
    const filter = $('#filter-input').val();
    sessionStorage.setItem('filter', filter);
    loadDataFromServer(filter);
}

function loadDataFromServer(filter) {
    settings.url = `${settings.url}&filter=${filter}`;

    $.ajax(settings).done(function (response) {
        populateTable(response);
        getServerPagination($('#pagnination'), $('#produtos'), api, response.quantidade, 10, populateTable, showError);
        Materialize.updateTextFields();
        $('#pages').on('change', function () {
            paginate(api, 10);
        }).material_select();
    });
    settings.url = `${api}?ppage=1&pquantidade=10`
}


function populateTable(response) {
    let rows = '';
    $.each(response.produtos, function (i, item) {
        rows += `<tr><td>${item.partNumber}</td><td>${item.descricao}</td><td class="money2">${item.valor}</td><td class="center-align"><a href="#" class="btn btn-default waves-effect" onclick="loadDetail('produtos/detail','${item.id}')"><i class="material-icons">visibility</i></a></td></tr>`;
    });
    $('#produtos').html(rows);
}

function showError(response, status) {
    toast('Falha ao buscar dados no servidor');
}
const apiCliente = "https://api-vertiv-biz.mybluemix.net/api/Cliente/mobile";
let selector = null;
const settingsListCliente = {
    "async": true,
    "crossDomain": true,
    "url": `${apiCliente}?ppage=1&pquantidade=20`,
    "method": "GET",
    "headers": {
        "Cache-Control": "no-cache",
        "authorization": `bearer ${JSON.parse(localStorage.getItem('token')).token}`
    }
};


$(document).ready(function () {
    selector = '#lista-clientes';
    $('#app-action').html('');

    setTitle('Clientes');
    if (sessionStorage.getItem('bot-cad-cliente') === 'true')
        $('#cnpj').val(sessionStorage.getItem('bot-cnpj-cliente'));

    $.ajax(settingsListCliente).done(function (response) {
        //loadingList(selector);
        populateList(response);

    });
    Materialize.updateTextFields();
});


function filterList() {
    const filter = $('#filter-input').val();
    sessionStorage.setItem('filter', filter);
    $(selector).html('');

    loadDataFromServer(filter);
}

function loadDataFromServer(filter) {

    settingsListCliente.url = `${settingsListCliente.url}&filter=${filter}`;
    $.ajax(settingsListCliente).done(function (response) {
        populateList(response);
        //getServerListPagination($('#pagnination'), $('#produtos'), api, response.quantidade, 10, populateTable, showError);
        Materialize.updateTextFields();
        //$('#pages').on('change', function () {
        //    paginate(api, 20);
        //}).material_select();
    });
    settingsListCliente.url = `${apiCliente}?ppage=1&pquantidade=20`
}

function populateList(response) {
    $.each(response.cliente, function (i, item) {
        $(`<li class="collection-item"><div><span>${item.nome} - ${item.cidade} ${item.estado}<br/><small>${item.cnpj}</small></span><a href="javascript:loadDetail('clientes/detail','${item.id}');" class="secondary-content"><i class="material-icons">keyboard_arrow_right</i></a></div></li>`).appendTo($('#lista-clientes'));
    });
    removeLoadingList(selector);
}

function validate() {

    let err = [];

    if ($('#cnpj').val() === '' || $('#cnpj').val() === 'undefined' || $('#cnpj').val() === null) {
        err.push("O campo [CNPJ] é obrigatório");
        setInvalid('#cnpj');
    }
    else {
        setValid('#cnpj');
    }
    if ($('#eMail').val() === '' || $('#eMail').val() === 'undefined' || $('#eMail').val() === null) {
        err.push("Preeencha o campo [E-mail] com um e-mail válido");
        setInvalid('#eMail');
    }
    else {
        setValid('#eMail');
    }
    if ($('#razaoSocial').val() === '' || $('#razaoSocial').val() === 'undefined' || $('#razaoSocial').val() === null) {
        err.push("O campo [Razão Social] é obrigatório");
        setInvalid('#razaoSocial');
    }
    else {
        setValid('#razaoSocial');
    }
    if ($('#nomeFantasia').val() === '' || $('#nomeFantasia').val() === 'undefined' || $('#nomeFantasia').val() === null) {
        err.push("O campo [Nome Fantasia] é obrigatório");
        setInvalid('#nomeFantasia');
    }
    else {
        setValid('#nomeFantasia');
    }
    if ($('#segmento').val() === '' || $('#segmento').val() === 'undefined' || $('#segmento').val() === null) {
        err.push("O campo [Segmento] é obrigatório");
        setInvalid('#cnpj');
    }
    else {
        setValid('#cnpj');
    }
    if ($('#inscricaoEstadual').val() === '' || $('#inscricaoEstadual').val() === 'undefined' || $('#inscricaoEstadual').val() === null) {
        err.push("O campo [Inscrição Estadual] é obrigatório");
        setInvalid('#segmento');
    }
    else {
        setValid('#segmento');
    }
    if ($('#inscricaoMunicipal').val() === '' || $('#inscricaoMunicipal').val() === 'undefined' || $('#inscricaoMunicipal').val() === null) {
        err.push("O campo [Inscrição Municipal] é obrigatório");
        setInvalid('#inscricaoMunicipal');
    }
    else {
        setValid('#inscricaoMunicipal');
    }
    if ($('#calaParent').val() === '' || $('#calaParent').val() === 'undefined' || $('#calaParent').val() === null) {
        err.push("O campo [CALA Parent] é obrigatório");
        setInvalid('#calaParent');
    }
    else {
        setValid('#calaParent');
    }
    if ($('#tipoCala').val() === '' || $('#tipoCala').val() === 'undefined' || $('#tipoCala').val() === null) {
        err.push("O campo [Tipo CALA] é obrigatório");
        setInvalid('#tipoCala');
    }
    else {
        setValid('#tipoCala');
    }
    if ($('#mercado').val() === '' || $('#mercado').val() === 'undefined' || $('#mercado').val() === null) {
        err.push("O campo [Mercado] é obrigatório");
        setInvalid('#mercado');
    }
    else {
        setValid('#tipoCala');
    }
    if ($('#nome').val() === '' || $('#nome').val() === 'undefined' || $('#nome').val() === null) {
        err.push("O campo [Nome do Contato] é obrigatório");
        setInvalid('#nome');
    }
    else {
        setValid('#nome');
    }
    if ($('#cidade').val() === '' || $('#cidade').val() === 'undefined' || $('#cidade').val() === null) {
        err.push("O campo [Cidade] é obrigatório");
        setInvalid('#cidade');
    }
    else {
        setValid('#cidade');
    }
    if ($('#estado').val() === '' || $('#estado').val() === 'undefined' || $('#estado').val() === null) {
        err.push("O campo [Estado] é obrigatório");
        setInvalid('#estado');
    }
    else {
        setValid('#estado');
    }
    if ($('#cep').val() === '' || $('#cep').val() === 'undefined' || $('#cep').val() === null) {
        err.push("O campo [CEP] é obrigatório");
        setInvalid('#cep');
    }
    else {
        setValid('#cep');
    }
    if (err.length > 0) {
        let erros = '';
        $.each(err, function (i, item) {
            erros += `${item}<br/>`
        });
        console.log(erros);
        toast(erros);
    }
    else {
        return true;
    }
}

function setInvalid(element) {
    $(element).addClass('invalid');
}

function setValid(element) {
    $(element).removeClass('invalid');
}

function saveCliente() {
    const valid = validate();
    console.log(`CLIENTE: ${valid}`);
    if (valid === true) {
        const cliente = {
            "cnpj": $('#cnpj').val(),
            "razaoSocial": $('#razaoSocial').val(),
            "nomeFantasia": $('#nomeFantasia').val(),
            "optanteSimplesNacional": "Não",
            "segmento": $('#segmento').val(),
            "contribuinteICMS": "Não",
            "inscricaoEstadual": $('#inscricaoEstadual').val(),
            "tipoCALA": $('#tipoCala').val(),
            "subframa": "0",
            "inscricaoMunicipal": $('#inscricaoMunicipal').val(),
            "calaParent": $('#calaParent').val(),
            "endereco": {
                "logradouro": $('#logradouro').val(),
                "bairro": $('#bairro').val(),
                "cidade": $('#cidade').val(),
                "numero": $('#numero').val(),
                "complemento": $('#complemento').val(),
                "estado": $('#estado').val(),
                "cep": $('#cep').val(),
                "complemento": $('#complemento').val()
            },
            "contato": {
                "ddiTelefone": null,
                "dddTelefone": null,
                "telefone": $('#telefone').val(),
                "ddiCelular": null,
                "dddCelular": null,
                "celular": $('#celular').val(),
            },
            "mercado": $('#mercado').val(),
            "escrVendas": $('#escritorioVendas').val(),
            "subComponente": $('#subComponente').val(),
            "orgVendas": "0067",
            "nomeContato": $('#nome').val(),
            "cargoContato": "Vendedor",
            "emailContato": $('#eMail').val(),
            "departamentoContato": "Vendas",
            "contatoContato": {
                "ddiTelefone": null,
                "dddTelefone": null,
                "telefone": $('#telefone').val(),
                "ddiCelular": null,
                "dddCelular": null,
                "celular": $('#celular').val()
            },
            "applicationUserClientes": null
        };

        console.log(JSON.stringify(cliente));
        const settings2 = getBaseSettings(getEndpoint('api/Cliente'), 'POST');
        settings2.data = JSON.stringify(cliente);

        $.ajax(settings2).done(function (response) {
            toast('Dados salvos com sucesso', 4000);
            if ($(document).width() <= 600)
                loadPage('clientes/index');
            else
                loadPage('clientes/table');
            if (sessionStorage.getItem('bot-cad-cliente') === 'true') {
                sessionStorage.removeItem('bot-cad-cliente');
                sendIntegrationMessage(cliente.cnpj);
                if (!$('.bot-wrapper').is(":visible"))
                    hideBot();
            }

        }).fail(function (response, status) {
            toast('Falha ao salvar os dados do cliente.', 4000);
        });
    }
}

function showError(response, status) {
    toast('Falha ao buscar dados no servidor');
}
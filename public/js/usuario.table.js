const apiUser = 'https://api-vertiv-biz.mybluemix.net/api/revenda/GetUsuariosRevenda/' + JSON.parse(localStorage.getItem('user')).revendas[0].cnpj;
const settings = {
    "async": true,
    "crossDomain": true,
    "url": `${apiUser}?ppage=1&pquantidade=10`,
    "method": "GET",
    "headers": {
        "Cache-Control": "no-cache",
        "authorization": `bearer ${JSON.parse(localStorage.getItem('token')).token}`,
        "Postman-Token": "6b6f0b3f-dbf9-c1a8-5c21-f0cfc2f72618"
    }
};

$(document).ready(function () {

    $('#app-action').html('<a href="javascript:openMainModal(\'#criarUsuario\')" class="btn waves-effect waves-light orange right">+ NOVO</a>');
    setTitle('Usuários');

    loadingTable($('#usuarios'));

    loadDataFromServer('');

});

function filterTable() {
    loadingTable($('#usuarios'));
    const filter = $('#filter-input').val();
    sessionStorage.setItem('filter', filter);
    loadDataFromServer(filter);
}

function loadDataFromServer(filter) {
    settings.url = `${settings.url}&filter=${filter}`;

    $.ajax(settings).done(function (response) {
        populateTable(response);
        getServerPagination($('#pagnination'), $('#usuarios'), apiUser, response.quantidade, 10, populateTable, showError);
        Materialize.updateTextFields();
        $('#pages').on('change', function () {
            paginate(apiUser, 10);
        }).material_select();
    });
    settings.url = `${apiUser}?ppage=1&pquantidade=10`
}


function populateTable(response) {
    console.log(response);
    let rows = '';
    $.each(response.usuarios, function (i, item) {
        rows += `<tr><td>${item.userName}</td><td>${item.email}</td><td>${item.cidade} - ${item.cidade}</td><td>${item.phoneNumber}</td></tr>`;
    });
    $('#usuarios').html(rows);
}

function showError(response, status) {
    toast('Falha ao buscar dados no servidor');
}

function createUser() {
    if ($('#senha').val() === $('#csenha').val()) {
        if ($('#senha').val() === '' || $('#email').val() === '') {
            toast('Todos os campos devem ser preenchidos');
        }
        else {

            const data = {
                email: $('#email').val(),
                cnpj: JSON.parse(localStorage.getItem('user')).revendas[0].cnpj,
                password: $('#senha').val()
            };

            const settings = {
                "async": true,
                "crossDomain": true,
                "url": "https://api-vertiv-biz.mybluemix.net/api/usuario/CadastrarUsuario",
                "method": "POST",
                "headers": {
                    "Token": JSON.parse(localStorage.getItem('token')).token,
                    "authorization": `bearer ${JSON.parse(localStorage.getItem('token')).token}`,
                    "Content-Type": "application/json",
                    "Cache-Control": "no-cache",
                    "Postman-Token": "f1f7ba21-7301-c6cc-9166-dd894aac45d0"
                },
                "processData": true,
                "data": JSON.stringify(data)
            };

            $.ajax(settings).done(function (response) {
                toast('Usuário cadastrado com sucesso.');
                closeModal();
                loadDataFromServer('');
            }).fail(function (response, status) {
                toast('Falha ao cadastrar o usuário.');
            });
        }
    }
    else {
        toast('A senha e a confirmação de senha estão diferentes.')
    }
}

function closeModal() {
    $('#criarUsuario').closeModal();
}
const apiProposta = "https://api-vertiv-biz.mybluemix.net/api/Proposta/mobile";
let selector = null;
const settingsListProposta = {
    "async": true,
    "crossDomain": true,
    "url": `${apiProposta}?ppage=1&pquantidade=20`,
    "method": "GET",
    "headers": {
        "Cache-Control": "no-cache",
        "authorization": `bearer ${JSON.parse(localStorage.getItem('token')).token}`
    }
};

$(document).ready(function () {
    selector = '#lista-propostas';

    $('#app-action').html('');
    $('.collapsible').collapsible();
    setTitle('Propostas');
    $.ajax(settingsListProposta).done(function (response) {
        //loadingList(selector);
        populateList(response);
    });
});


function filterList() {
    const filter = $('#filter-input').val();
    sessionStorage.setItem('filter', filter);
    $(selector).html('');

    loadDataFromServer(filter);
}

function loadDataFromServer(filter) {

    //loadingList(selector);
    settingsListProposta.url = `${settingsListProposta.url}&filter=${filter}`;
    $.ajax(settingsListProposta).done(function (response) {
        populateList(response);
        //getServerListPagination($('#pagnination'), $('#produtos'), api, response.quantidade, 10, populateTable, showError);
        Materialize.updateTextFields();
        //$('#pages').on('change', function () {
        //    paginate(api, 20);
        //}).material_select();
    });
    settingsListProposta.url = `${apiProposta}?ppage=1&pquantidade=20`
}

function populateList(response) {
    $.each(response.propostas, function (i, item) {
            let nome = item.nomeProjeto;
            if (nome === null || nome === '')
                nome = 'Proposta Sem Nome';
            $(`<li><div class="collapsible-header"><i class="material-icons">description</i><span class="truncate">${item.nProposta} - ${nome}</span></div><div class="collapsible-body" style="background-color: #fafafa;"><div class="row"><div class="col s12"><a href="#" onclick="loadDetail('propostas/detail','${item.id}')" class="right"><i class="material-icons">chevron_right</i></a>${item.nomeCliente}<br/>${parseFloat(item.valor).toLocaleString('pt-BR',  { minimumFractionDigits: 2 , style: 'currency', currency: 'BRL' })}<br/> Proposta ${item.objSituacao.value}</div><div class="col s2 center"><i class="material-icons grey-text" style="font-size: small;">access_time</i></div><div class="col s2 center"><i class="material-icons grey-text" style="font-size: small;">mail_outline</i></div><div class="col s2 center"><i class="material-icons grey-text" style="font-size: small;">system_update_alt</i></div><div class="col s2 center"><i class="material-icons grey-text" style="font-size: small;">autorenew</i></div><div class="col s2 center"><i class="material-icons grey-text" style="font-size: small;">local_shipping</i></div><div class="col s2 center"><i class="material-icons grey-text" style="font-size: small;">done</i></div><div class="col s12 center"><div class="progress white" style="border-bottom: 1px lightgray;"><div class="determinate orange" style="width: ${((item.objStatus.key / 6) * 100) + 5}%"></div></div></div></div></div></div></li>`).appendTo($('#lista-propostas'));
        });
    removeLoadingList(selector);
}

function showError(response, status) {
    toast('Falha ao buscar dados no servidor');
}

let objCliente = {};


$(document).ready(function () {
    loadPage('blank');
    validateLogin();
    if ($(window).width < 600) {
        let viewport = document.querySelector("meta[name=viewport]");
        viewport.setAttribute('content', 'width=device-width, initial-scale=0.8, maximum-scale=1.0, user-scalable=0');
        $('.bot-body').width($(window).width() - 20);
    }
    $('#app-action').html('');
    $('.bot-wrapper').toggle(100);
    $('.bot-view').height($(document).height() - 130);
    $('#menu-activator').sideNav({
        menuWidth: 300, // Default is 240
        edge: 'left', // Choose the horizontal origin
        closeOnClick: true // Closes side-nav on <a> clicks, useful for Angular/Meteor
    });
    loadMenu();
    loadBotDesk();
    loadPage('dashboard/index');
    registerNotification();
    registerFormat();
    if ($('#menu-item-bot').is(":visible"))
        $('#tgt-desk').tapTarget('open');

    $('.modal').modal();
});

function hideBot() {
    $('.bot-wrapper').toggle(500);
}

function loadMenu() {
    $.get(`views/menu/header.html?rld=${getUid()}`, function (header) {
        $.get(`views/menu/menu.html?rld=${getUid()}`, function (menu) {
            $('.side-nav').html(header + menu);
        });
    });
}

function loadBot() {
    $.get(`views/chat.html?rld=${getUid()}`, function (data) {
        $('.bot').html(data);
    });
}

function loadPage(view) {

    validateLogin();
    $.get(`views/${view}.html?rld=${getUid()}`, function (data) {
        $('.view').html(data);
    });

}

function loadDetail(view, id) {

    validateLogin();
    if (view.includes('produtos')) {
        $(document).ready(function () {
            const settings = {
                "async": true,
                "crossDomain": true,
                "url": `https://api-vertiv-biz.mybluemix.net/api/Produto/mobile/${id}`,
                "method": "GET",
                "headers": {
                    "Cache-Control": "no-cache",
                    "Postman-Token": "49e263fb-5824-b2e1-26c7-7627a7685a00"
                }
            };

            $.ajax(settings).done(function (response) {
                localStorage.setItem('produto', JSON.stringify(response.produto));
                $.get(`views/${view}.html?rld=${getUid()}`, function (data) {
                    $('.view').html(data);
                });
            });
        });
    }
    else if (view.includes('solucoes')) {
        $(document).ready(function () {
            const settings = {
                "async": true,
                "crossDomain": true,
                "url": `https://api-vertiv-biz.mybluemix.net/api/SolutionBox/mobile/${id}`,
                "method": "GET",
                "headers": {
                    "Cache-Control": "no-cache",
                    "Postman-Token": "49e263fb-5824-b2e1-26c7-7627a7685a00"
                }
            };

            $.ajax(settings).done(function (response) {
                localStorage.setItem('solucao', JSON.stringify(response.solutionBox));
                $.get(`views/${view}.html?rld=${getUid()}`, function (data) {
                    $('.view').html(data);
                });
            });
        });
    }
    else if (view.includes('clientes')) {
        $(document).ready(function () {
            const settings = {
                "async": true,
                "crossDomain": true,
                "url": `https://api-vertiv-biz.mybluemix.net/api/cliente/mobile/${id}`,
                "method": "GET",
                "headers": {
                    "Cache-Control": "no-cache",
                    "Postman-Token": "49e263fb-5824-b2e1-26c7-7627a7685a00"
                }
            };

            $.ajax(settings).done(function (response) {
                sessionStorage.setItem('cliente', JSON.stringify(response.cliente));
                $.get(`views/${view}.html?rld=${getUid()}`, function (data) {
                    $('.view').html(data);
                });
            });
        });
    }
    else if (view.includes('propostas')) {
        $(document).ready(function () {
            const settings = {
                "async": true,
                "crossDomain": true,
                "url": `https://api-vertiv-biz.mybluemix.net/api/proposta/mobile/${id}`,
                "method": "GET",
                "headers": {
                    "Cache-Control": "no-cache",
                    "authorization": `bearer ${JSON.parse(localStorage.getItem('token')).token}`,
                    "Postman-Token": "49e263fb-5824-b2e1-26c7-7627a7685a00"
                }
            };

            $.ajax(settings).done(function (response) {
                sessionStorage.setItem('proposta', JSON.stringify(response.propostas));

                settings.url = `https://api-vertiv-biz.mybluemix.net/api/proposta/ConsultaProposta/${response.propostas.nProposta}`;
                $.ajax(settings).done(function (response1) {
                    console.log(response1);
                    sessionStorage.setItem('proposta-lista-comicao', JSON.stringify(response1));

                    $.get(`views/${view}.html?rld=${getUid()}`, function (data) {
                        $('.view').html(data);
                    });
                });
            });
        });
    }
}

function validateLogin() {
    var localTime = new Date().getTime();


    if (localStorage.getItem('token') !== null) {

        const token = JSON.parse(localStorage.getItem('token'));
        //var diff = token.issued - localTime;
        if (true) {
            var settings = {
                "async": true,
                "crossDomain": true,
                "url": "https://api-vertiv-biz.mybluemix.net/account/refreshtoken",
                "method": "GET",
                "headers": {
                    "Content-Type": "application/json",
                    "Cache-Control": "no-cache",
                    "authorization": `bearer ${JSON.parse(localStorage.getItem('token')).token}`,
                },
                "processData": false
            }

            $.ajax(settings).done(function (response) {
                let mili = new Date().getTime();
                mili = mili + (24 * 60 * 1000);
                localStorage.setItem('token', JSON.stringify({token: response.token, issued: mili}));
            });

        }
        if (token.issued < localTime)
            if (!window.location.href.toLowerCase().includes('login.html'))
                logout();
    }
    else if (!window.location.href.toLowerCase().includes('login.html'))
        window.location.href = 'login.html';
}

function logout() {
    localStorage.removeItem('token');
    localStorage.clear();
    window.location.href = `login.html?rld=${getUid()}`;
}

function setTitle(title) {
    $('#app-title').html(title);
}

function loadBotDesk() {
    $.get(`views/chat/index.html?rld=${getUid()}`, function (data) {
        $('.bot-view').html(data);
    });
}

function toogleMenu() {
    /*
        if ($('#slide-out').attr('style').includes('display: none;'))
            $('#slide-out').attr('style', $('#slide-out').attr('style').toString().replace('display: none;', ''));

        let sidePosition = $('#slide-out').position().left;
        let pos = sidePosition === 0 ? -310 : 0;
        $('#slide-out').animate({"left": pos}, 100);
        */
    $('#menu-activator').sideNav('show');
}

function toast(message) {
    Materialize.toast(message, 4000);
}

function getUid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }

    return `${s4() + s4()}-${s4()}-${s4()}-${s4()}-${s4()}${s4()}${s4()}`;
}

function alterPassword() {
    if ($('#novaSenha').val() === $('#confirmarSenha').val()) {
        if ($('#novaSenha').val() === '') {
            toast('Todos os campos devem ser preenchidos');
        }
        else {

            const data = {
                email: JSON.parse(localStorage.getItem('user')).email,
                password: $('#novaSenha').val()
            };

            const settings = {
                "async": true,
                "crossDomain": true,
                "url": "https://api-vertiv-biz.mybluemix.net/api/usuario/AlterarSenha",
                "method": "POST",
                "headers": {
                    "Token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX0lEIjoiMTAwMDU2IiwidXN1YXJpbyI6Im5leG9AdmVydGl2Y28uY29tIn0.WnA5Uegzg0ueinjGYF2Y1RXwdb6LrB1BkKMYJ4XO_6g",
                    "Content-Type": "application/json",
                    "Cache-Control": "no-cache",
                    "authorization": `bearer ${JSON.parse(localStorage.getItem('token')).token}`
                },
                "processData": true,
                "data": JSON.stringify(data)
            };

            $.ajax(settings).done(function (response) {
                toast('Senha alterada com sucesso.');
            }).fail(function (response, status) {
                toast('Falha ao alterar a senha.');
            });
        }
    }
    else {
        toast('A senha e a confirmação de senha estão diferentes.')
    }
}

function openMainModal(element) {
    $(element).modal('open');
}

function closeMainModal() {
    $('.modal').modal('close');
}

function hideBot() {
    $('.bot-wrapper').toggle(500);
}


function searchCliente(identifier) {
    if($(identifier).val() === '' || $(identifier).val() === null)
        return false;

    $('#finalidadeCopia').html('');
    $('select').material_select('destroy');
    $('select').material_select();
    const settings = {
        "async": true,
        "crossDomain": true,
        "url": `https://api-vertiv-biz.mybluemix.net/api/Cliente/mobile/buscacnpj?cnpj=${$(identifier).val()}`,
        "method": "GET",
        "headers": {
            "Cache-Control": "no-cache",
            "Postman-Token": "ed4e2df6-1016-dabb-8e6a-447bfa192eb2"
        }
    };

    $.ajax(settings).done(function (response) {
        let objCliente = response.cliente[0];
        if(objCliente.cnpj === JSON.parse(localStorage.getItem('user')).revendas[0].cnpj)
        {
            $('#finalidadeCopia').html('<option value="CONSUMO">Consumo Próprio</option>\n');
            $('select').material_select('destroy');
            $('select').material_select();
        }
        else{
            $('#finalidadeCopia').html('<option value="REVENDA">Venda Direta</option>\n' + '<option value="INDUSTRIALIZAÇÃO">Industrialização</option>\n');
            $('select').material_select('destroy');
            $('select').material_select();
        }
        $('#nomeClienteCopia').val(objCliente.razaoSocial).change();
        $('#idClienteSolucao').val(objCliente.id).change();

    }).fail(function (response, status) {
        toast('CNPJ inválido ou não localizado.');
    });
}


function registerNotification(){
    const config = {
        apiKey: "AIzaSyBqEgJCfmqAgyNM0n1EyOZYio4XhxZqzIw",
        authDomain: "vertiv-f9d75.firebaseapp.com",
        databaseURL: "https://vertiv-f9d75.firebaseio.com",
        projectId: "vertiv-f9d75",
        storageBucket: "vertiv-f9d75.appspot.com",
        messagingSenderId: "111136780710"
    };
    firebase.initializeApp(config);
}

function registerFormat() {
    if (!String.prototype.format) {
        String.prototype.format = function() {
            let formatted = this;
            for (let i = 0; i < arguments.length; i++) {
                let regexp = new RegExp('\\{'+i+'\\}', 'gi');
                formatted = formatted.replace(regexp, arguments[i]);
            }
            return formatted;
        }
    }
}



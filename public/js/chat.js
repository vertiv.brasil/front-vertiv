let d = 0, h = 0, m = 0, i = 0;
let context = null;
let state = 'hide';
const apiRoot = 'https://api-vertiv-biz.mybluemix.net/api';
const apiBot = 'https://api-vertiv-biz.mybluemix.net/api/application/conversation';
let lists = {};
let cards = {};
let msg = null;

$(document).ready(function () {
    if ($(document).width <= 600) setTitle('Assistente Virtual');
    if($(document).width <600){
        viewport = document.querySelector("meta[name=viewport]");
        viewport.setAttribute('content', 'width=device-width, initial-scale=0.8, maximum-scale=1.0, user-scalable=0');
    }
    else{

        viewport = document.querySelector("meta[name=viewport]");
        viewport.setAttribute('content', 'width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=0');
    }

    setTimeout(function () {
        $('.bot-panel').hide();
    }, 100);
    $('.tooltipped').tooltip({delay: 50});
    $('.bot-btn').on('click', function () {
        if (state === 'hide') {
            $('.bot-panel').show(1000);
            startBot();
            state = 'show'
        } else {
            $('.bot-panel').hide(1000);
            state = 'hide'
        }
    });
    $('.message-submit').on('click', function () {
        insertMessage();
    });
    $('.message-input').on('keypress', function (e) {
        if (e.which === 13) {
            insertMessage();
        }
    });
    $('#modal-detail').modal();
    sendMessage('start');
});

function setDate() {
    d = new Date();
    m = d.getMinutes();
    if (m <= 9) m = `0${m}`;
    return `<div class="timestamp">${d.getHours()}:${m}</div>`;
}

function insertMessage() {
    msg = $('.message-input').val();
    if ($.trim(msg) === '') {
        return false;
    }
    $(`<div class="col s12"><div class="col s2 text-left timer">${setDate()}</div><div class="mine col s10">${msg}</div></div>`).appendTo($('.bot-body')).addClass('new');
    $('.message-input').val(null);
    sendMessage(msg);
    scrollMessages();
}

function insertFormatedMessage(type,condition,formatText,expression) {
    let oldMsg = $('.message-input').val().replace('.',',');
    let msg = '';


    if ($.trim(oldMsg) === '') {
        return false;
    }

    if(type === 'pattern' || 'expression'){
        const regex = new RegExp(`${condition}`,'i');
        let res = oldMsg.match(regex);
        if(res!== null && res.length > 0)
            msg = formatText.format(res[0]);
        if ($.trim(msg) === '') {
            toast('O valor informado não é válido.');
            return false;
        }

        if(type === 'expression') {
            let codeUid = getUid().replace(/-/g, '_');
            let exp = expression.format(msg, codeUid);
            executeStringCode(exp);
        }
        else{
            sendFormatedMessage(msg)
        }
    }

/*
    $(`<div class="col s12"><div class="col s2 text-left timer">${setDate()}</div><div class="mine col s10">${msg.replace('_',' ')}</div></div>`).appendTo($('.bot-body')).addClass('new');
    $('.message-input').val(null);
    sendMessage(msg);
    scrollMessages();*/
}

function executeStringCode(code) {
    try {
        setTimeout(eval(code.toString()),1);
    }
    catch (ex){
        toast('Expressão inválida.');
    }
}

function sendFormatedMessage(msg){
    $(`<div class="col s12"><div class="col s2 text-left timer">${setDate()}</div><div class="mine col s10">${msg.replace('_',' ')}</div></div>`).appendTo($('.bot-body')).addClass('new');
    $('.message-input').val(null);
    sendMessage(msg);
    scrollMessages();
}

function isEmail(email) {
    return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email);
}

function startBot() {
    context = null;
    $('.bot-body').html('');
    sendMessage('start');
}

function sendIntegrationMessage(textToSend) {
    context = JSON.parse(sessionStorage.getItem('bot-context'));
    context.action = {data: null, type: "validate-cnpj", style: null, text: null};
    sendMessage(textToSend)
}

function sendMessage(textToSend) {
    removeInputStyle();
    validateLogin();
    const settings = {
        "async": true,
        "crossDomain": true,
        "url": apiBot,
        "method": "POST",
        "headers": {
            "content-type": "application/json",
            "cache-control": "no-cache",
            "authorization": `bearer ${JSON.parse(localStorage.getItem('token')).token}`,
            "postman-token": "3b6719f1-baed-39f0-d89c-5f6c1f7f522e"
        },
        "processData": false,
        "data": `{"text":"${textToSend}", "context":${JSON.stringify(context)}}`
    };
    console.log(settings);
    $.ajax(settings).done(function (data) {
        console.log(data);
        context = data.context;
        $('.message.loading').remove();
        if (data.context.action.style === 'form-cadastro_cliente_completo') {
            sessionStorage.setItem('bot-cad-cliente', 'true');
            sessionStorage.setItem('bot-context', JSON.stringify(context));
            sessionStorage.setItem('bot-cnpj-cliente', data.context.CNPJ);
            loadPage('clientes/add');
        } else if (data.context.action.style === 'form-consultaPreco') {
            let forms = genFormTabelaPrecos(data.context.listaprecos, data.context.cliente);
            $(forms).appendTo($('.bot-body'));
            scrollMessages();
        } else if (data.context.action.style === 'form-solutionbox') {
            let forms = genFormSolutionBox(data.context.solutionbox);
            $(forms).appendTo($('.bot-body'));
            scrollMessages();
        } else if (data.context.action.style === 'form-proposta') {
            let forms = genFormProposta(data.context.propostas);
            $(forms).appendTo($('.bot-body'));
            scrollMessages();
        } else {
            $.each(data.context.action.text, function (i, item) {
                $(`<div class="col s12"><div class="well col s10 resp">${item}</div><div class="col s2 timer">${setDate()}</div></div>`).appendTo($('.bot-body'));
                setDate();
                scrollMessages();
            });
            console.log(data.context.action);
            if (data.context.action.fieldStyle != null){
                performInputStyle(data.context.action.fieldStyle);
            }
            if (data.context.action.style === 'card') {
                const id = getUid();
                cards[id] = {total: data.context.action.cardinformation.length, cards: [], position: 0};
                localStorage.setItem('cards', null);
                let index = 0;
                $.each(data.context.action.cardinformation, function (i, item) {
                    let img =  (item.imagem === null || item.imagem === '') ? 'img/prod_padrao.png': item.imagem;
                    const litem = genCard(`${item.modelo} - ${item.titulo}`, item.partnumber, img, `javascript:showDetail('${item.id}')`, `javascript:addItem('${item.id}')`, index);
                    console.log(litem);
                    cards[id].cards.push(litem);
                    index++;
                });
                localStorage.setItem('cards', JSON.stringify(cards));
                let btn_right = `<a onclick="moveCardRight('${id}');"class="btn btn-circle waves-effect waves-light orange" style="position:relative; margin-right:10px;margin-top:45%"><i class="material-icons">chevron_right</i></a>`;
                let btn_left = `<a onclick="moveCardLeft('${id}');" class="btn btn-circle waves-effect waves-light orange" style="position:relative; margin-left:10px;margin-top:45%"><i class="material-icons">chevron_left</i></a>`;
                if (cards[id].total === 1) {
                    btn_left = '&nbsp;';
                    btn_right = '&nbsp;';
                }
                $(`<div class="row"><div class="col s2">${btn_left}</div><div class="col s8" id="card_${id}">${cards[id].cards[0]}</div><div class="col s2">${btn_right}</div></div>`).appendTo($('.bot-body'));
                scrollMessages();
            }
            if (data.context.action.data) {
                let btns = '<div class="collection">';
                $.each(data.context.action.data, function (i, item) {
                    btns += ` <a class="collection-item orange-text chat-btn" href="javascript:sendMessage('${item.payload}');">${item.title}</a>`;
                });
                btns += '</div><div class="col s12">&nbsp;</div> ';
                $(btns).appendTo($('.bot-body'));
                scrollMessages();
            }
        }
    });
}

function moveCardLeft(id) {
    cards = JSON.parse(localStorage.getItem('cards'));
    console.log(cards[id]);
    if (cards[id].position >= (cards[id].total - 1)) {
        cards[id].position -= 1;
        console.log(cards[id].position);
        // $('#card_' + id).html(cards[id].cards[cards[id].position]);
        $('#card_' + id).fadeOut(500, function () {
            $('#card_' + id).html(cards[id].cards[cards[id].position]);
            $('#card_' + id).fadeIn(500);
        });
    }
    localStorage.setItem('cards', JSON.stringify(cards));
}

function moveCardRight(id) {
    cards = JSON.parse(localStorage.getItem('cards'));
    console.log(cards[id]);
    if (cards[id].position < (cards[id].total - 1)) {
        cards[id].position += 1;
        console.log(cards[id].position);
        // $('#card_' + id).html(cards[id].cards[cards[id].position]);
        $('#card_' + id).fadeOut(500, function () {
            $('#card_' + id).html(cards[id].cards[cards[id].position]);
            $('#card_' + id).fadeIn(500);
        });
    }
    localStorage.setItem('cards', JSON.stringify(cards));
}

function genCard(title, description, image, action1, action2, index) {
    return `<div class="col s12"><div class="thumbnail"><img src="${image}" class="col s12" alt="${title}"><div class="caption"><h5>${title}</h5><p>${description}</p><p><a href="${action1}" class="btn btn-primary" role="button"><i class="material-icons">remove_red_eye</i></a> <a href="${action2}" class="btn btn-default" role="button"><i class="material-icons">add_box</i></a></p></div></div></div>`;
}

function genFormSolutionBox(items) {
    const uid = getUid();
    let form = `<span class="card-title">Salvar Solução</span><div class="row"><div class="input-field col s12"><label for="nomeSolucao${uid}" class="active">Nome do Projeto</label><input class="form-control" id="nomeSolucao${uid}" name="nomeProposta" placeholder="Nome do Projeto" type="text"></div><div class="input-field col s12"><select id="visibilidade${uid}" name="finalidade"><option value="1">Pública</option><option value="2">Privada</option><option value="3">Compartilhada</option></select><label for="visibilidade${uid}">Visibilidade</label></div><div class="col s12" style="width:100%"><ul class="collapsible" data-collapsible="expandable"><li id="collapse-f6587ffe-2e95-6072-62dc-4b25a134a623" class="panel-collapse collapse"><div class="collapsible-header"><i class="material-icons">swap_vert</i>Itens da Solução</div><div class="collapsible-body"><ul class="collection"><li class="collection-item" style="max-height: 250px; overflow-y: scroll">`;
    lists[uid] = [];
    let index = 0;
    $.each(items, function (i, item) {
        lists[uid].push(item);
        form += `<div class="row smoke"><div class="col s6 text-vertical"><strong class="title">PN: ${item.partNumber}</strong></div><div class="col s2"><a href="#"onclick="decreaseItemSolucao('${uid}',${index})"class="text-red"><i class="material-icons">remove</i></a></div><div class="col s2 text-vertical">QTD: <span id="qtd_${uid}_${index}">${item.quantidade}</span></div><div class="col s2"><a href="#"onclick="increaseItemSolucao('${uid}',${index})"class="text-red"><i class="material-icons">add</i></a></div><div class="col s12 divider"></div><div class="col s12">${item.descricao}</div></div>`;
        index++;
    });
    form += `</li></ul></div></li></ul></div></div><div class="card-action" style="padding-right: 20px;"><button id="btn_solution${uid}" type="submit" onclick="saveSolution('${uid}')"class="btn btn-default">Salvar</button></div><script>$(document).ready(function () {$('.collapsible').collapsible();$('select').material_select();});</script>`;
    localStorage.setItem(uid, JSON.stringify(lists[uid]));
    return form;
}

function saveSolution(uid) {
    const btn = `#btn_solution${uid}`;
    const nome = $(`#nomeSolucao${uid}`).val();
    if (nome === '' || nome === 'undefined' || nome === null) {
        toast('Todos os campos do formulário precisam ser preenchidos.', 4000)
    } else {
        $(btn).text('SALVANDO');
        $(btn).attr('disabled', 'disabled');
        let cnpjRevenda = null;
        if (JSON.parse(localStorage.getItem('user')).revendas !== null && JSON.parse(localStorage.getItem('user')).revendas.length > 0) cnpjRevenda = JSON.parse(localStorage.getItem('user')).revendas[0].cnpj;
        const solution = {
            "descricao": $(`#nomeSolucao${uid}`).val(),
            "partNumber": uid,
            "visibilidade": $(`#visibilidade${uid}`).val(),
            "dataInicial": JSON.parse(JSON.stringify(new Date())),
            "dataFinal": null,
            "cnpjRevenda": cnpjRevenda,
            "favorito": false,
            "itensSolutionBox": JSON.parse(localStorage.getItem(uid))
        };
        console.log(solution);
        const settings = {
            "async": true,
            "crossDomain": true,
            "url": `${apiRoot}/solutionbox`,
            "method": "POST",
            "headers": {
                "Content-Type": "application/json",
                "Cache-Control": "no-cache",
                "authorization": `bearer ${JSON.parse(localStorage.getItem('token')).token}`,
                "Postman-Token": "eed32d3d-5aef-3881-b3d3-bb0fab7bc06a"
            },
            "processData": false,
            "data": JSON.stringify(solution)
        };
        $.ajax(settings).done(function (response) {
            $(btn).text('SALVO');
            $(btn).attr('disabled', 'disabled');
            console.log(response);
            toast('Solução salva com sucesso.');
            if ($(document).width() > 600) loadPage('solucoes/table');
            sendMessage('encerramento-solution_box')
        }).fail(function (response, status) {
            $(btn).text('SALVAR');
            $(btn).removeAttr('disabled');
            toast(response.responseJSON.retorno.error)
        });
    }
}

function genFormProposta(proposta) {
    const uid = getUid();
    const usr = JSON.parse(localStorage.getItem('user'));
    let form = `<span class="card-title">Salvar Proposta</span><div class="row"><div class="col s12"><strong>${proposta.nomeCliente}<input value="${proposta.idCliente}" id="idCliente${uid}"type="hidden"></strong><br>${proposta.cnpj}</div><div class="input-field col s12"><label for="nomeProposta${uid}" class="active">Nome do Projeto</label><input class="form-control" id="nomeProposta${uid}" name="nomeProposta" placeholder="Nome do Projeto" type="text"></div><div class="input-field col s12"><select id="finalidade${uid}" name="finalidade">`;
    if (proposta.cnpj.replace('.', '').replace('-', '').replace('/', '') === (usr.revendas.length > 0 && usr.revendas[0].cnpj.replace('.', '').replace('-', '').replace('/', ''))) form += '<option value="CONSUMO">Consumo Próprio</option>\n';
    else form += '<option value="REVENDA">Venda Direta</option>\n' + '<option value="INDUSTRIALIZACAO">Industrialização</option>\n';
    form += `</select><label for="finalidade${uid}">Finalidade do Projeto</label></div><div class="input-field col s12"><label for="nomeContato${uid}">Nome do Contato</label><input class="form-control" id="nomeContato${uid}" name="nomeContato${uid}" placeholder="Nome do Contato" type="text"></div><div class="input-field col s12"><label for="email${uid}">Email do Contato</label><input class="form-control" id="email${uid}" name="emailContato${uid}" placeholder="E-mail do Contato" type="email"></div><div class="col s12" style="width:100%"><ul class="collapsible" data-collapsible="expandable"><li id="collapse-f6587ffe-2e95-6072-62dc-4b25a134a623" class="panel-collapse collapse"><div class="collapsible-header"><i class="material-icons">swap_vert</i>Itens da Proposta</div><div class="collapsible-body"><ul class="collection"><li class="collection-item" style="min-height: 250px;">`;
    lists[uid] = [];
    let index = 0;
    $.each(proposta.produtos, function (i, item) {
        lists[uid].push(item);
        form += `<div class="row smoke"><div class="col s6 text-vertical"><strong class="title">PN: ${item.produto.partNumber}</strong></div><div class="col s2"><a href="#"onclick="decreaseItem('${uid}',${index})"class="text-red"><i class="material-icons">remove</i></a></div><div class="col s2 text-vertical">QTD: <span id="qtd_${uid}_${index}">${item.qtde}</span></div><div class="col s2"><a href="#"onclick="increaseItem('${uid}',${index})"class="text-red"><i class="material-icons">add</i></a></div><div class="col s12 divider"></div><div class="col s12">${item.produto.descricao}</div></div>`;
        index++;
    });
    form += `</li></ul></div></li></ul></div></div><div class="card-action" style="padding-right: 20px;"><button id="btn_proposta${uid}" type="submit" onclick="saveProposta('${uid}')"class="btn btn-default">Salvar</button></div><script>$(document).ready(function () {$('.collapsible').collapsible(); $('select').material_select();});</script>`;
    localStorage.setItem(uid, JSON.stringify(lists[uid]));
    return form;
}

function saveProposta(uid) {
    const btn = `#btn_proposta${uid}`;
    const nome = $(`#nomeProposta${uid}`).val();
    const email = $(`#email${uid}`).val();
    const contato = $(`#nomeContato${uid}`).val();
    if (nome === '' || nome === 'undefined' || nome === null || email === '' || email === 'undefined' || email === null || (isEmail(email) === false) || contato === '' || contato === 'undefined' || contato === null) {
        toast('Todos os campos do formulário precisam ser preenchidos.');
        if ((isEmail(email) === false)) toast('O campo email não está preenchido corretamente.');
    } else {
        $(btn).text('SALVANDO');
        $(btn).attr('disabled', 'disabled');
        const solution = {
            "idcliente": $(`#idCliente${uid}`).val(),
            "nomeProjeto": $(`#nomeProposta${uid}`).val(),
            "codPessoaContato": null,
            "canalDistribuicao": "21",
            "status": 1,
            "situacao": 1,
            "nomeContato": $(`#nomeContato${uid}`).val(),
            "percentualComissao": "10.5",
            "finalidade": $(`#finalidade${uid}`).val(),
            "contato": {
                "ddiTelefone": "+55",
                "dddTelefone": "",
                "telefone": "",
                "ddiCelular": null,
                "dddCelular": null,
                "celular": null,
                "email": $(`#emailContato${uid}`).val()
            },
            "produtos": JSON.parse(localStorage.getItem(uid))
        };
        const settings = {
            "async": true,
            "crossDomain": true,
            "url": `${apiRoot}/proposta`,
            "method": "POST",
            "headers": {
                "Content-Type": "application/json",
                "Cache-Control": "no-cache",
                "Postman-Token": "eed32d3d-5aef-3881-b3d3-bb0fab7bc06a",
                "authorization": `bearer ${JSON.parse(localStorage.getItem('token')).token}`
            },
            "processData": false,
            "data": JSON.stringify(solution)
        };
        $.ajax(settings).done(function (response) {
            console.log(response);
            $(btn).text('SALVO');
            $(btn).attr('disabled', 'disabled');
            if ($(document).width() > 600) loadPage('propostas/table');
            sendMessage('encerramento-pr-sucesso');
            toast('Proposta salva com sucesso.');
        }).fail(function (response, status) {
            $(btn).text('SALVAR');
            $(btn).removeAttr('disabled');
            toast(response.responseJSON.retorno.error);
        });
    }
}

function genFormTabelaPrecos(proposta, cliente) {
    const uid = getUid();
    let form = `<div class="card" style="margin-left: 4px; margin-right: 4px"><div class="card-content"><span class="card-title">Consulta de Preços</span></div><div class="row"><div class="col s12"><strong>${cliente.nome}<input value="${cliente.id}" id="idCliente${uid}"type="hidden"></strong><br>${cliente.cnpj}</div> </div> <ul class="collection">`;
    $.each(proposta, function (i, item) {
        form += `<li class="collection-item"><span class="title">${item.descricao}</span><p><b>PN: ${item.partNumber}</b><br>R$:<span> ${item.valor.toLocaleString('pt-BR')}</span></p></li>`;
    });
    form += '</ul>' + '<div class="card-action" style="padding-right: 20px;">\n' + '<button type="submit" onclick="sendMessage(\'start\')"\n' + 'class="btn btn-default">Encerrar\n' + '</button>\n' + '</div>\n' + '</div>\n' + '<script>\n' + '$(document).ready(function () {\n' + '$(\'.collapsible\').collapsible();\n' + '$(\'select\').material_select();\n' + '});\n' + '\n' + '</script>';
    return form;
}

function addItem(id) {
    sendMessage(id)
}

function increaseItem(uid, index) {
    const obj = JSON.parse(localStorage.getItem(uid));
    obj[index].qtde++;
    localStorage.setItem(uid, JSON.stringify(obj));
    $(`#qtd_${uid}_${index}`).html(obj[index].qtde);
}

function decreaseItem(uid, index) {
    const obj = JSON.parse(localStorage.getItem(uid));
    if (obj[index].qtde > 0) {
        obj[index].qtde--;
        localStorage.setItem(uid, JSON.stringify(obj));
        $(`#qtd_${uid}_${index}`).html(obj[index].qtde);
    }
}

function increaseItemSolucao(uid, index) {
    const obj = JSON.parse(localStorage.getItem(uid));
    obj[index].quantidade++;
    localStorage.setItem(uid, JSON.stringify(obj));
    $(`#qtd_${uid}_${index}`).html(obj[index].quantidade);
}

function decreaseItemSolucao(uid, index) {
    const obj = JSON.parse(localStorage.getItem(uid));
    if (obj[index].quantidade > 0) {
        obj[index].quantidade--;
        localStorage.setItem(uid, JSON.stringify(obj));
        $(`#qtd_${uid}_${index}`).html(obj[index].quantidade);
    }
}

function scrollMessages() {
    const wtf = $('.bot-body');
    const height = wtf[0].scrollHeight;
    wtf.scrollTop(height);
}

function showDetail(idProduto) {
    const settings = {
        "async": true,
        "crossDomain": true,
        "url": `https://api-vertiv-biz.mybluemix.net/api/Produto/mobile/${idProduto}`,
        "method": "GET",
        "headers": {
            "Cache-Control": "no-cache",
            "authorization": `bearer ${JSON.parse(localStorage.getItem('token')).token}`
        }
    };
    $.ajax(settings).done(function (response) {
        $('#modal-detail').modal('open');
        $('#detail-title').html(response.produto.descricao);
        let items = '';
        $.each(response.produto.atributos, function (i, item) {
            items += `<li class="collection-item"><h5>${item.valor}<br/><small>${item.label}</small></h5></li>`;
        });
        $('#detail-list').html(items);
    });
}

function performInputStyle(style) {
    $('#input_text').off('keypress');
    $('.message-input').on('keypress', function (e) {
        if (e.which === 13) {
            insertFormatedMessage(style.type, style.condition, style.format, style.expression);
        }
    });
}

function removeInputStyle() {
    if (context != null && context.action != null) {
        context.action.fieldStyle = null;
    }
    $('#input_text').off('keypress');
    $('.message-input').on('keypress', function (e) {
        if (e.which === 13) {
            insertMessage();
        }
    });
}
var proposta = {};
var comissoes = [];
var objComissoes = {};
var lkey = '';


function getPosition(value, array, key) {
    let pos = 0;
    $.each(array, function (i, item) {
        if (item[key] === value)
            return pos;
        else
            pos++;
    });
}


function loadData() {
    let lists = {};
    let uid = '1';
    let index = 0;

    lists[uid] = [];

    $('#nProposta').val(proposta.nProposta);
    $('#nomeProjeto').val(proposta.nomeProjeto);
    $('#finalidade').val(proposta.objFinalidade.key).change();
    $('#cnpj').val(proposta.cnpj);
    $('#nomeCliente').val(proposta.nomeCliente);
    $('#valor').val(parseFloat(proposta.valor.toString().replace(',', '')).toLocaleString('pt-BR', {
        minimumFractionDigits: 2,
        style: 'currency',
        currency: 'BRL'
    }));
    $('#comissao').val(proposta.valorComissao.toLocaleString('pt-BR', {
        minimumFractionDigits: 2,
        style: 'currency',
        currency: 'BRL'
    }));
    $('.dropdown-button').dropdown({
        inDuration: 300,
        outDuration: 225,
        constrainWidth: false, // Does not change width of dropdown to that of the activator
        hover: true, // Activate on hover
        gutter: 12, // Spacing from edge
        belowOrigin: true, // Displays dropdown below the button
        alignment: 'right', // Displays dropdown with edge aligned to the left of button
        stopPropagation: false // Stops event propagation
    });
    $.each(proposta.produtos, function (i, item) {
        lists[uid].push(item);
        const litem = `<li class="collection-item"><div class="row smoke"><div class="col s6 text-vertical"><strong class="title">PN: ${item.produto.partNumber}</strong></div>
<div class="col s2 text-vertical">
Vlr.:  ${item.valorBruto.toLocaleString('pt-BR', {minimumFractionDigits: 2, style: 'currency', currency: 'BRL'})}</div>
<div class="col s4 text-vertical">
    QTD: <span id="qtd_${index}">${item.qtde}</span>
</div>
<div class="col s12 divider"></div>
<div class="col s12">${item.produto.descricao}</div>
</div></li>`;
        $(litem).appendTo('#itemsProposta');
        index++;
    });
    $.each(comissoes, function (i, item) {
        let key = item.value.toString().replace('.', '_');
        objComissoes[key] = {
            totalPrice: item.totalPrice,
            totalRebate: item.totalRebate,
            rebate: item.value
        };
    });
    $('#comicao').attr('min', 1);
    $('#comicao').attr('max', comissoes.length);
    $('#comicao').attr('step', 1);
    lkey = proposta.percentualComissao.toString().replace('.', '_');
    var rebate = objComissoes[lkey].rebate;
    $('#comicao').val(getPosition(rebate, comissoes, 'value') + 1);
    setTitle(`Detalhe da Proposta ${proposta.nProposta} (${proposta.objSituacao.value})`);
    getMenuOpcoes();
    updateRevision(lkey);
}

function getMenuOpcoes() {
    const mnuFeedback = proposta.objSituacao.key === 1 ? `<li><a id="mnu-opt-feedback-prop" class="orange-text"  href="#" onclick="openFeedback();">FEEDBACK</a></li>` : '';
    const mnuDownload = proposta.objSituacao.key <= 2 ? `<li><a id="mnu-opt-download-prop" class="orange-text" target="_blank" href="${proposta.download}" id="btn-download-proposta">DOWNLOAD</a></li>` : '';
    const mnuEmail = proposta.objSituacao.key <= 2 ? `<li><a id="mnu-opt-email-prop" class="orange-text" href="#" onclick="sendPropostaByMail();">EMAIL</a></li>` : '';
    const mnuRevisao = proposta.objSituacao.key === 1 ? `<li><a id="mnu-opt-revisao-prop" class="orange-text" href="#" onclick="openRevisao();">REVISÃO</a></li>` : '';
    const mnuKickOff = proposta.objSituacao.key === 2 ? `<li><a id="mnu-opt-kickoff-prop" class="orange-text" href="#" onclick="openKickOff('${proposta.id}');">KICK OFF</a></li>` : '';

    $('#app-action').html(`<a class="dropdown-button btn orange" href="#" data-activates="dropdown1" style="width: 120px">OPÇÕES<i class="material-icons right">arrow_drop_down</i></a>
    <!-- Dropdown Structure -->
    <ul id="dropdown1" class="dropdown-content">
        ${mnuDownload}
        ${mnuEmail}
        <li><a id="mnu-opt-copia-prop" class="orange-text" href="#" onclick="openCopia();">COPIAR</a></li>
        ${mnuRevisao}
        ${mnuFeedback}
        ${mnuKickOff}
    </ul>`);


    $('.dropdown-button').dropdown({
            inDuration: 300,
            outDuration: 225,
            constrainWidth: false, // Does not change width of dropdown to that of the activator
            hover: true, // Activate on hover
            gutter: 0, // Spacing from edge
            belowOrigin: false, // Displays dropdown below the button
            alignment: 'left', // Displays dropdown with edge aligned to the left of button
            stopPropagation: false // Stops event propagation
        }
    );

}

function showVal(obj) {
    console.log(obj);
    const lkey = comissoes[parseInt(obj) - 1].value.toString().replace('.', '_');
    updateRevision(lkey);
}

function openFeedback() {
    if (proposta.objSituacao.key === 1)
        $('#modalFeedback').modal('open');
    else
        toast('Já foi enviado um feedback para esta proposta.')
}

function openKickOff(id) {
    toast('Favor aguardar enquanto a sua requisição está sendo processada.');
    /*localStorage.setItem('proposta', JSON.stringify(proposta));
    if (proposta.objSituacao.key === 5) {
        const settings = {
            "async": true,
            "crossDomain": true,
            "url": "https://api-vertiv-biz.mybluemix.net/api/proposta/"+proposta.nProposta,
            "method": "GET",
            "headers": {
                "content-type": "application/json",
                "authorization": `bearer ${JSON.parse(localStorage.getItem('token')).token}`,
                "cache-control": "no-cache",
            },
            "processData": false,
            "data": ''
        };

        $.ajax(settings).done(function (data) {
            sessionStorage.setItem('kickoff',JSON.stringify(data));
            $.get(`views/propostas/kickoff.html?rld=${getUid()}`, function (data) {
                $('.view').html(data);
            });
        }).fail(function (response, status) {
            toast(response.responseJSON.retorno.error)
        })
    }
    else*/
        $.get(`views/propostas/kickoff.html?rld=${getUid()}`, function (data) {
            $('.view').html(data);
        });
}

function openRevisao() {
    $('#modalRevisao').modal('open');
}

function openCopia() {
    $('#modalCopia').modal('open');
}

function closeModal() {

    $('#modalFeedback').modal('close');
    $('#modalRevisao').modal('close');
    $('#modalCopia').modal('close');
    $('#motivo').val('');
    $('#feedbackStatus').val('').change();
}

function sendFeedback() {
    toast('Favor aguardar enquanto a sua requisição está sendo processada.');
    const settings = {
        "async": true,
        "crossDomain": true,
        "url": "https://api-vertiv-biz.mybluemix.net/api/proposta/feedbackup",
        "method": "POST",
        "headers": {
            "content-type": "application/json",
            "authorization": `bearer ${JSON.parse(localStorage.getItem('token')).token}`,
            "cache-control": "no-cache",
            "postman-token": "3b6719f1-baed-39f0-d89c-5f6c1f7f522e"
        },
        "processData": false,
        "data": `{ "idProposta":"${proposta.id}","observacao":"${$('#motivo').val()}","status":"${$('#feedbackStatus').val()}"}`
    };

    $.ajax(settings).done(function (data) {
        toast('Feedback enviado com sucesso.');
        if ($(document).width() > 600) loadPage('propostas/table');
        else loadPage('propostas/index');
    }).fail(function (response, status) {
        toast(response.responseJSON.retorno.error)
    }).always(function (data) {
        $('#motivo').val('');
        $('#feedbackStatus').val('').change();
        $('#modalFeedback').modal('close');
    });
}

function sendRevisao() {
    toast('Favor aguardar enquanto a sua requisição está sendo processada.');
    const lkey = comissoes[parseInt($('#comicao').val())].value.toString().replace('.', '_');
    proposta.percentualComissao = objComissoes[lkey].rebate * 1.0;

    const obj = {
        id: proposta.id,
        nProposta: proposta.nProposta,
        idcliente: proposta.idCliente,
        nomeProjeto: proposta.nomeProjeto,
        codPessoaContato: proposta.codPessoaContato,
        canalDistribuicao: proposta.canalDistribuicao,
        status: proposta.objStatus.key,
        situacao: proposta.objSituacao.key,
        nomeContato: proposta.contato.nomeContato,
        finalidade: proposta.objFinalidade.value.toUpperCase(),
        percentualComissao: proposta.percentualComissao,
        contato: proposta.contato,
        produtos: proposta.produtos
    };

    const lsettings = {
        "async": true,
        "crossDomain": true,
        "url": "https://api-vertiv-biz.mybluemix.net/api/proposta/RevisaoPropostaSap",
        "method": "POST",
        "headers": {
            "Token": JSON.parse(localStorage.getItem('token')).token,
            "authorization": `bearer ${JSON.parse(localStorage.getItem('token')).token}`,
            "Content-Type": "application/json",
            "Cache-Control": "no-cache",
            "Postman-Token": "a27a4ee2-af33-7a35-11e6-b5e7aa20b5c2"
        },
        "processData": false,
        "data": JSON.stringify(obj)
    };

    $.ajax(lsettings).done(function (response) {
        toast('Revisao enviada com sucesso.');
        $('#modalFeedback').modal('close');
        if ($(document).width() > 600)
            loadPage('propostas/table');
        else
            loadPage('propostas/index');
    }).fail(function (response, status) {
        toast(response.responseJSON.retorno.error)
    });
}

function sendCopia() {
    toast('Favor aguardar enquanto a sua requisição está sendo processada.');
    let nProposta = {
        nProposta: proposta.nProposta,
        idcliente: $('#idClienteSolucao').val(),
        nomeProjeto: $('#nomeProjetoCopia').val(),
        codPessoaContato: null,
        canalDistribuicao: "21",
        status: 1,
        situacao: 1,
        nomeContato: $('#nomeContatoCopia').val(),
        finalidade: $('#finalidadeCopia').val(),
        contato: {
            ddiTelefone: null,
            dddTelefone: null,
            telefone: null,
            ddiCelular: null,
            dddCelular: null,
            celular: null,
            email: $('#emailContatoCopia').val()
        },
        produtos: proposta.produtos

    };

    console.log(nProposta);
    const settings = {
        "async": true,
        "crossDomain": true,
        "url": "https://api-vertiv-biz.mybluemix.net/api/proposta/CopiaPropostaSap",
        "method": "POST",
        "headers": {
            "Token": JSON.parse(localStorage.getItem('token')).token,
            "authorization": `bearer ${JSON.parse(localStorage.getItem('token')).token}`,
            "Content-Type": "application/json",
            "Cache-Control": "no-cache",
            "Postman-Token": "a27a4ee2-af33-7a35-11e6-b5e7aa20b5c2"
        },
        "processData": false,
        "data": JSON.stringify(nProposta)
    };

    $.ajax(settings).done(function (response) {
        toast(`Cópia gerada com sucesso.`);

        if ($(document).width() > 600) loadPage('propostas/table');
        else loadPage('propostas/index');
    }).fail(function (response, status) {
        toast(response.responseJSON.retorno.error)
    });

}

function calculate(id) {

    if (id === 'comicao') {

        const lkey = comissoes[parseInt($('#comicao').val())].value.toString().replace('.', '_');
        updateRevision(lkey);

    }
}

function updateRevision(lkey) {
    $('span.thumb').html(`<span class="rebate">${(objComissoes[lkey].rebate * 1.0).toLocaleString('pt-BR')}%</span>`);
    $('#perComissao').html((objComissoes[lkey].rebate * 1.0).toLocaleString('pt-BR', {minimumFractionDigits: 2}));
    $('#valComissao').html((objComissoes[lkey].totalRebate * 1.0).toLocaleString('pt-BR', {minimumFractionDigits: 2}));
    $('#valProposta').html((objComissoes[lkey].totalPrice * 1.0).toLocaleString('pt-BR', {minimumFractionDigits: 2}));
}

function sendPropostaByMail() {
    const settings = {
        "async": true,
        "crossDomain": true,
        "url": "https://api-vertiv-biz.mybluemix.net/api/proposta/sendmail",
        "method": "POST",
        "headers": {
            "Token": JSON.parse(localStorage.getItem('token')).token,
            "authorization": `bearer ${JSON.parse(localStorage.getItem('token')).token}`,
            "Content-Type": "application/json",
            "Cache-Control": "no-cache",
            "Postman-Token": "a27a4ee2-af33-7a35-11e6-b5e7aa20b5c2"
        },
        "processData": false,
        "data": JSON.stringify({
            nPoposta: proposta.nProposta,
            email: JSON.parse(localStorage.getItem('user')).email,
            fileUrl: proposta.download
        })
    };

    $.ajax(settings).done(function (response) {
        toast(`Proposta enviada com sucesso para o e-mail ${JSON.parse(localStorage.getItem('user')).email}.`);
    }).fail(function (response, status) {
        toast(`Falha ao enviar a proposta por e-mail ${JSON.parse(localStorage.getItem('user')).email}.`);
    });
}

$(document).ready(function () {
    $('#app-action').html('');
    proposta = JSON.parse(sessionStorage.getItem('proposta'));
    comissoes = JSON.parse(sessionStorage.getItem('proposta-lista-comicao')).consulta.rebate;
    console.log(lkey);
    if (proposta !== null) {
        loadData();
        lkey = proposta.percentualComissao.toString().replace('.', '_');
        Materialize.updateTextFields();
        $('select').material_select();
        $('.modal').modal();
    }
    else loadPage('propostas/index');
});
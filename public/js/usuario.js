let base64image = '';
let base64file = '';
const user = JSON.parse(localStorage.getItem('user'));
$(document).ready(function () {

    let imgUrl = '';
    if(user.imageUrl === null || user.imageUrl === '')
        imgUrl = "img/no-profile-pic.png";
    else
        imgUrl = user.imageUrl;

    $('#app-action').html('');
    $('#imageWrapper').html(`<img id="image" src="${imgUrl}">`);
    $('#telefone').val(user.telefone);
    $('#cidade').val(user.cidade);
    $('#estado').val(user.estado);
    Materialize.updateTextFields();
    setTitle('Meus Dados');
    $('#imagePath').on('change', function () {
        const files = document.getElementById('userPic').files;
        if (files.length > 0) {
            getBase64(files[0]);
        }
    });
});

function getBase64(file) {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
        base64image = reader.result;
        base64file = base64image.split(',')[1];
        $('#imageWrapper').html('<img id="image" src="'+base64image+'">');
        console.log(base64image);
    };
    reader.onerror = function (error) {
        console.log('Error: ', error);
    };
}

function saveUserData() {
    const data = JSON.parse(localStorage.getItem('user'));
    data.image = base64file;

    const settings = {
        "async": true,
        "crossDomain": true,
        "url": `https://api-vertiv-biz.mybluemix.net/api/usuario/${data.id}`,
        "method": "PUT",
        "headers": {
            "Content-Type": "application/json",
            "Cache-Control": "no-cache",
            "authorization": `bearer ${JSON.parse(localStorage.getItem('token')).token}`,
            "Postman-Token": "1fa2b6ff-d698-0f69-7e2d-de74de0bb8db"
        },
        "processData": false,
        "data": JSON.stringify(data)
    };

    $.ajax(settings).done(function (response) {
        console.log(response);
        $('#hdr_img').attr('src',$('#image').attr('src'));
        toast('Salvo com sucesso.');
    })
        .fail(function (response, status) {
            toast(response.retorno.erro);
        });
}